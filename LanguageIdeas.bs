/*
B-Sharp

------------Goals-------------
Make a language that is far less verbose than C++, but doesn't compromise on performance. It is a
compiled language, that has functionality for all major programming concepts (oop, reflection, etc.)
Everything can be programmed in a platform independent way, but will have to be re-compiled for each
platform (like C++). Can be optimized per platform. Breaking interface on many libraries will occur
to improve usability/security/performance/stability, but only on major builds. This will not allow
backwards compatibility, but will greatly improve previously stated things. This will also reduce
the amount of legacy code that will be converted to run on newer versions. Legacy code SHOULD NOT be
used, so this strategy would be used (I know this greatly reduces the target audience). Compile
times are highly weighted with the 
structure of this language.

----included functionality----
networking
events
condition_variable's
string (unicode? what?)
clock stuff
console in/out
version stuff
file i/o
USB?
blue-tooth?
smart pointers and normal pointers
built in unit testing
built in documenting
arrays have lengths properties
gui library? (maybe use existing cross platform library?)
gpu acceleration library (maybe special syntax?)
asynchronous syntax????
threadpool
json library
serializable objects?

----------properties----------
files are a scope
only exported functions/classes/etc can be seen from outside an included file and even then they
	-have to be explicitly included
an int can be broken down into individual bit accesses using C struct-like syntax called "register"
support for 128, 256, etc. bit numbers?
use UInt64, Int16, Float32, etc for primitive values
casting between Int, Float, etc, does not convert values, but instead just reinterprets the data
less verbose templates
compiling can compile each file without knowing any others into some sort of byte code
compiling a project is a simple as telling the compiler to compile the main file
interfaces can be handled implicitly, have syntax for explicitly retracting interface. explicitly
	-specify interfaces function names
has anonymous functions/function variables that can be passed in-line
single file for functionality. No header/cpp file crap
compiler can generate "header"-like file that only has interface in text, not implementation in text
	-(.lib?)
can use DLL's, but has alternate functionality for "C*" "DLL"s
supports operator overriding. including multi-dimensional array syntax - [,,,]
macros are only defined in that scope, and can be multi-line, but are still pre-processor
supports by default, macros for functionName, lineNumber, fileName, debug/release etc.
mutexing a property, function, or class
mutex that supports read/write/?? mutexing
definition order doesn't matter. if something is defined lower in a file, it can still be used
	-higher. Only scope matters
can define functions for data outside of the class/data use special keyword "supporting?" Can be
	-used for specific
primitives have class like syntax for manipulating them ex. int.toFloat64()
supports class extension, but does it support multiple extensions?
figure out better constructor copy vs. execute on object
struct can extend from class, and vice-versa maybe? I think not?
function definitions are simplified
Error Handling????
Includes auto benchmarking/Big-O,Little Omega, etc. analysis???
Documentation???
Some way to indicate to optimize each instance at compile time
indicate difference between array (contains length etc.) and a string of ints, chars, objects.
allow easy memory benchmarking for each object/function/etc.
visibility = public/private/internal(only internal classes etc. can see the items)/owner(only parent
	-class etc can see the items)
Use preprocessor directives to evaluate to sections of code, and choose the most efficient (target =
	-inline assembly)
preprocessor case statements
scope operator that evaluates preprocessor
handle changing interface?
profiler???
mark function with fast keyword, to designate, that it should make the function as fast as possible,
	-without regard to memory consumption
includes/imports/whatever are scoped, so you can expose the included stuff to outside files or not 
	-(default private)
"this" is a reference, not a pointer, to the item
import statements can rename what they are using. e.g. "using std::string as Alternate_String". This
	-will not expose the first name to the scope, only the second.
"static" isn't stupid
easy way to serialize data using json library???
find a way to import package from web (git or something), so that builds are always up to date. have
	- a modified syntax for external includes/imports, maybe #include [https://a.com/dump]
Compiled reflection???
embed data in exe?
non-platform specific way of communicating with another program
non-platform specific way of recieving/sending events through the system?
Auto generate code. ability to get a list of object members/functions (macros?)
scoped pointer (more generic than pointer?)
enums are scoped to the enum name???
define operators on members of a class/struct
defined console macros for certain paths, that are cross platform
tag conditional blocks with assumed percent chance, to allow better optimization of compiled code
syntax to run constructor on object intead of running on new object, and copying to variable
debug functions for reporting states
all types are implicitly forward declared, so you never need to make anything in a particular order
class cannot be defined across multiple files. It must have all information in a single file?
ifdef/ifndef/elseif macros can be replaced with standard if/else and will be optimized to get rid of
	-the invalidated case. Use special syntax for evaluating expression as predefined macro
realloc with new size like C realloc. I like this feature of C
define partial constructor to construct a full object with unique defaults
Visual Leak Detector built in.
Auto-styling based on selected style type.
macros that can optimize the code for specific instances?
enum that multiplies by 2 instead of adds one (001 010 100) for the values
somehow use javascripts syntax for || where it returns the first non-zero value
user can generate copy constructor so that a subclass/parentClass can be copied to from each other?
cross platform installer?
arrays have bounds checking (raw pointers do not)
imports/includes/using directives are scoped
somehow enforce rule of a class can only manage one resource
enforce rule of three? (copy constructor, copy assignment operator, destructor)
"this" is a reference to the current object instead of a pointer to it
Should it create default copy/move/etc functions, or should it force the user to create their own?
how to handle const better? maybe split up definitions (won't allow it to be changed by function/
	-won't be changed by whoever has access to it)
assume const unless otherwise specified?
hints to optimize for size/performance on a per object/per function basis?
copy constructor can be used to create new objects
create local variable of base class, but can store any subclass, but would need to be as large as
	-the largest subclass (No Pointers!!!)
scoped pointer
improve sizeof operator to include managed resources (allow tag for owned resources?, or alternate
	-function for appending size of other data)
Allow some sort of ability to read call stack (maybe only in debug? unless otherwise specified)
allow exception forwarding (catch and throw new exception)
Static Constructor like C#
Different compilers can provide different defaults. e.g. Micro-controllers would not want some of
	- the niceties
Specify Empty? constructor that sets everything to some representation of nothing.
Types are a type. Type intType = Int;
Default pass by value NOT PASS BY REFERENCE LIKE C#
functions, variables, clases and such have functions to retrieve their names (maybe not variables...)
Inheritance that doesn't use the function names specified, but instead uses its own  with references
    - to the inherited class's functions
Alternative pointers, variants of safe pointers
standard pointer: @, e.g. Int@ = @b.Int()
https://www.reddit.com/r/ProgrammingLanguages/comments/dwjnzk/creating_a_pointer_that_enforces_only_one/


-------need to research-------
templates only being compiled once? How does java compile generics?
reflection

-------important terms--------
class-		normal definition, is allowed to rearrange terms for better memory size/access speed
struct-		normal definition, is NOT allowed to rearrange terms for better memory size/access speed
register-	defines "struct" like syntax that contains bitwise data



-----------examples-----------
*/

//TODO: function declaration/inline definition
ReturnType func(Int a,Int b){return ReturnType;}//definition
func(Int, Int){ReturnType}//declaration/type

register[sizeof(uint32)] DacRegister
{
	[0]		runFlag:
	[1]		RESERVED:
	[2]		errorOccured
	[3]		RESERVED:
	[4-16]	buffer:

	bool	run()
	{
		runFlag = 1;
		
		if(errorOccured)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	/*implicit members*/
	/*
		runFlag.mask
		runFlag.operator[size_t?]? gives value of bit/ stores information at bit
		runFlag.bitShift
	*/
	
}

supporting Int
{
	MyClass	toMyClass()
	{
		return MyClass(this);
	}
}

mutexed public class ExampleClass<A, Thing>
{
	private:
	
	Thing member;
	A other;
	Float thingy;
	
	ExampleClass(A first, Thing second)
	{
		other 	= first;
		member 	= second;
	}
	
	mutexed/*unnecessary*/ public void foo()
	{
		
	}
	
	public Int [A a, Float b, Thing c]
	{
		
	}
	
	public ExampleClass += (int a)
	{
		
	}
	
}

supporting ExampleClass<A, Thing>
{
	ExampleClass<A, Thing> ExampleClass2(A first)
	{
		return ExampleClass(first, Thing(/*default*/));
	}
}

// example of macros being built in???
if(defined(debug))// if debug is false (not defined)
{
	std::cout << "testing value";// optimized out if debug isn't defined
}

// if verbosity or verbosity_secondary are not defined "definedEvaluation()" will evaluate as false
if(definedEvaluation(verbosity, verbosity_secondary; verbosity == verbosity_secondary == 3))
{
	//...
}

// example of simplified syntax for one-line catch statements?
data.get("thing") save(Exception ex){};

// example of using different name for Interface function
Class A : InterfaceB
{
    int foo() : InterfaceB.bar
    {
        
    }
}

// example of singleton pointer
Int* a = new Int(5)
foo(a) // foo now gets a, and a is set to NULL

// second example
foo()
{
    Type* result = new Type();
    // do stuff
    return result;
}

// third example
bar()
{
    Type* a = foo()
    // do stuff with a
    
    // a gets deleted on return
}






























