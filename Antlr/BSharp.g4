grammar BSharp;

/* Parser Rules */

program             : (white_space? declaration_expression white_space?)+ EOF ;
declaration_expression: (import_statement 
                    | variable_declaration
                    | function_declaration
                    | structure_declaration) ;
import_statement    : IMPORT_TEXT white_space string white_space? SEMICOLON ;
function_declaration: function_heading white_space? function_body ;
variable_declaration: type_name white_space variable_name white_space? (EQUAL white_space? valued_expression white_space?)? SEMICOLON ;
structure_declaration: class_declaration ; // TODO: add stucts, supporting, and registers
class_declaration   : class_header white_space? class_body ;
class_header        : CLASS_TEXT white_space? class_name ;
class_name          : LABEL ; // TODO: 
class_body          : OPEN_BRACKET white_space? (class_expression white_space?)* CLOSE_BRACKET ;
class_expression    : declaration_expression ; // TODO: Make useful
function_heading    : type_name white_space function_name white_space? OPEN_PAREN white_space? (function_parameters white_space?)? CLOSE_PAREN ;
function_parameters : function_parameter (white_space? COMMA white_space? function_parameter)* ;
function_parameter  : type_name white_space parameter_name (white_space? EQUAL white_space? valued_expression)? ;
function_body       : OPEN_BRACKET white_space? (function_expression white_space? SEMICOLON white_space?)* return_statement? white_space? CLOSE_BRACKET ;
parameter_name      : LABEL ; // TODO: 
return_statement    : RETURN_TEXT (white_space? valued_expression)? white_space? SEMICOLON ;
function_name       : LABEL ; // TODO: add scope
function_expression : declaration_expression | function_call | non_valued_expression | assignment_expression ;
function_call       : function_name white_space OPEN_PAREN (white_space valued_expression (white_space COMMA white_space valued_expression)* white_space)? CLOSE_PAREN ;
valued_expression   : function_call | number; // TODO: add other stuff
non_valued_expression: function_call ; // TODO: add other stuff
type_name           : LABEL ; // TODO:
assignment_expression: variable_name white_space? EQUAL white_space? valued_expression white_space? SEMICOLON ;
variable_name       : LABEL ; // TODO: add scope
string              : STRING ;
number              : NUMBER ;
white_space         : (comment | MULTILINE_COMMENT | WHITE_SPACE)+ ;
comment             : (LINE_COMMENT | MULTILINE_COMMENT) ;



fragment LOWERCASE  : [a-z] ;
fragment UPPERCASE  : [A-Z] ;
fragment NUM        : [0-9] ;
fragment NEWLINE    : ('\n' | '\r') ;
LINE_COMMENT        : '//' .*? NEWLINE ;
MULTILINE_COMMENT   : '/*' .*? '*/' ;
STRING              : '"' (LOWERCASE | UPPERCASE | NUM)* '"' ; // [^'\\"']
CLASS_TEXT          : 'class' ;
RETURN_TEXT         : 'return';
IMPORT_TEXT         : 'import' ;
NUMBER              : '-'? [0-9]*('.'[0-9]+) | '-'?[0-9]+ ;
LABEL               : (LOWERCASE | UPPERCASE | '_')+ ;
WHITE_SPACE         : (' ' | '\r' | '\t' | '\n');
OPEN_PAREN          : '(' ;
CLOSE_PAREN         : ')' ;
OPEN_BRACKET        : '{' ;
CLOSE_BRACKET       :'}' ;
SEMICOLON           : ';' ;
COMMA               : ',' ;
EQUAL               : '=' ;

