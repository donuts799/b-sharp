:: Create Java Parser for debugging purposes
..\\jdk\\bin\\java -cp "lib/*" org.antlr.v4.Tool -o output\Java\thisIsntUsed ..\\BSharp.g4

:: Compile the Java generated Parser
..\\jdk\\bin\\javac -cp "lib/*" output\\Java\\BSharp*.java

:: Create C++ Parser
..\\jdk\\bin\\java -cp "lib/*" org.antlr.v4.Tool -Dlanguage=Cpp -visitor -o output\C++\thisIsntUsed ..\\BSharp.g4