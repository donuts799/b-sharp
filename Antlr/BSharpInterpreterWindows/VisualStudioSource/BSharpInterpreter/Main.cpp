#include <iostream>
#include "antlr4-runtime.h"
#include "BSharpLexer.h"
#include "BSharpParser.h"
#include "UnitTest.h"
#include "BSharpInterpreter.h"

int main(int argc, char*argv[])
{
	if (argc < 2) exit(1);
	std::cout << "Compiling: " << argv[1] << std::endl;
	std::ifstream file = std::ifstream(argv[1]);
	BSharpInterpreter interpreter = BSharpInterpreter();
	interpreter.processInput(file);
}