#pragma once
#include <string>
#include <vector>

namespace Backend
{
	class StorageType
	{
		std::string type;
	public:
		StorageType(std::string typeString);
	};

	class BSharp_Item
	{
	public:
		BSharp_Item();
		virtual ~BSharp_Item();
		virtual StorageType getType();
	};
}

