#pragma once
#include "TestFramework.h"
class UnitTest
{
public:
	UnitTest();
	~UnitTest();
	void Test_Initialize_Backend();
	void Test_CreateVariable_Backend();
	void Test_CreateFunction_Backend();
private:
	TestFramework::Test testFramework;

};

extern UnitTest unitTest;

