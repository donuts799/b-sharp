#pragma once
#include "BSharp_Item.h"
#include "BSharp_Variable.h"

namespace Backend
{
	class BSharp_Function :
		public BSharp_Item
	{
	private:
		std::string name;
		std::vector<std::string> parameters;
	public:
		BSharp_Function();
		~BSharp_Function();
	};
}


