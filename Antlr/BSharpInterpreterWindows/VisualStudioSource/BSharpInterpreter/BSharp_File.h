#pragma once
#include <string>
#include <vector>
#include "BSharp_Item.h"
#include "BSharp_Variable.h"

namespace Backend
{
	class BSharp_File : BSharp_Item
	{
	protected:
		std::vector<std::shared_ptr<BSharp_Item>> itemList;
	public:
		BSharp_File();

		std::shared_ptr<BSharp_Item> storeItem(std::shared_ptr<BSharp_Item> item);
	};
}

