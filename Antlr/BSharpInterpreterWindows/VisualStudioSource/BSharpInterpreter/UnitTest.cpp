#include <sstream>
#include "UnitTest.h"
#include "BSharp_Function.h"
#include "BSharp_File.h"

using namespace TestFramework;
using namespace Backend;

UnitTest unitTest = UnitTest();

void UnitTest::Test_Initialize_Backend()
{
	try
	{
		BSharp_File storage;
	}
	catch (...)
	{
		Failed expression = Failed("Could not initialize Backend for unknown reasons");
		testFramework.test(expression, "Test_InitializeBackend()", "Backend");
	}
	Succeeded expression = Succeeded("Successfully initialized Backend");
	testFramework.test(expression, "Test_InitializeBackend()", "Backend");
}

void UnitTest::Test_CreateVariable_Backend()
{
	BSharp_File storage;
	try
	{
		BSharp_Variable(StorageType("Int32"), std::vector<std::string>());
	}
	catch (...)
	{
		Failed expression = Failed("Could not create a variable for unknown reasons");
		testFramework.test(expression, "Test_CreateVariable_Backend()", "Backend");
	}
	Succeeded expression = Succeeded("Successfully created variable");
	testFramework.test(expression, "Test_CreateVariable_Backend()", "Backend");
}

void UnitTest::Test_CreateFunction_Backend()
{
	try
	{
		BSharp_Function();
	}
	catch (...)
	{
		Failed expression = Failed("Could not create a function for unknown reasons");
		testFramework.test(expression, "Test_CreateFunction_Backend()", "Backend");
	}
	Succeeded expression = Succeeded("Successfully created variable");
	testFramework.test(expression, "Test_CreateFunction_Backend()", "Backend");
}

UnitTest::UnitTest()
{
	Test_Initialize_Backend();
	Test_CreateVariable_Backend();
	Test_CreateFunction_Backend();
}


UnitTest::~UnitTest()
{
}
