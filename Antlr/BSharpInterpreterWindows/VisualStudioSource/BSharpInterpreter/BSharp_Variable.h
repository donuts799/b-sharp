#pragma once
#include "BSharp_Item.h"

namespace Backend
{
	class BSharp_Variable : public BSharp_Item
	{
	private:
		StorageType type;
		std::string name;

		std::vector<std::string> options; // const, final, etc. it is a vector to be expandable
		StorageType getType();

	public:
		BSharp_Variable(StorageType type, std::vector<std::string> options = std::vector<std::string>());
	};
}
