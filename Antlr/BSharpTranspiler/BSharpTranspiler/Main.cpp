#include <iostream>
#include "antlr4-runtime.h"
#include "BSharpLexer.h"
#include "BSharpParser.h"
// #include "BSharpBaseVisitor.h"
#include "GetStream.h"
#include "Visitor.h"
#include "UnitTest.h"

int main(int argc, char*argv[])
{
	if (argc < 2)
	{
		std::cout << "Please include command line parameters" << std::endl;
		exit(1);
	}
	/*std::cout << "Compiling: " << argv[1] << std::endl;
	std::ifstream file = std::ifstream(argv[1]);
	if (!file)
	{
		std::cout << "File could not be opened" << std::endl;
	}*/

	auto file = getStream(StreamType::File, argv[1]);
	antlr4::ANTLRInputStream input = antlr4::ANTLRInputStream(*file);
	//antlr4::ANTLRFileStream antlrFile = antlr4::ANTLRFileStream(,file);
	BSharpLexer lexer(&input);
	antlr4::CommonTokenStream tokens(&lexer);
	BSharpParser parser(&tokens);

	BSharpParser::ProgramContext* tree = parser.program();
	std::cout << tree->toStringTree(&parser) << std::endl;
	
	Visitor visitor;
	antlrcpp::Any result = visitor.visitProgram(tree);

	antlrcpp::Any one = 1;
	if (one.is<int>())
	{
		std::cout << (int)one.as<int>();
	}
	antlrcpp::Any two = "myString";
	result.is<bool>();
}