#pragma once
#include <string>
#include <iostream>
#include <map>

namespace TestFramework
{
	class Expression
	{
	protected:
		std::string expressionString	= "";
		std::string message				= "";
		bool		value				= false;

		void process(std::string evaluationType, std::string left, std::string right, bool value, std::string message = "")
		{
			expressionString = evaluationType + "(" + left + "," + right + ")";
			this->message	+= message;
			this->value		 = value;
		}

	public:
		std::string getExpressionString()
		{
			return expressionString;
		}
		std::string getMessage()
		{
			return message;
		}
		bool isVerified()
		{
			return value;
		}

	};

	template <typename ExpressionType>
	class IsEqual : public Expression
	{
	public:
		IsEqual(ExpressionType left, ExpressionType right)
		{
			 process("IsEqual", std::to_string(left), std::to_string(right), left == right);
		}
	};
    
    template <typename ExpressionType>
	class IsNotEqual : public Expression
	{
	public:
		IsNotEqual(ExpressionType left, ExpressionType right)
		{
			 process("IsNotEqual", std::to_string(left), std::to_string(right), left != right);
		}
	};

	class IsNotNull: public Expression
	{
	public:
		IsNotNull(void* data)
		{
			process("IsNotNull", (data == NULL ? "NULL": "Not NULL"), "N/A", data != NULL);
		}
	};

	class Failed : public Expression
	{
	public:
		Failed(std::string errorMessage)
		{
			process("Failed", "N/A", "N/A", false, errorMessage);
		}
	};

	class Succeeded : public Expression
	{
	public:
		Succeeded(std::string successMessage)
		{
			process("Succeeded", "N/A", "N/A", true, successMessage);
		}
	};

	class Test
	{
	public:
		void test(Expression expression, std::string functionName = "", std::string groupName = "")
		{
			std::string conditionString;
			conditionString = std::string(expression.isVerified() ? "Passed" : "Failed") + " ";
			std::string outputString = conditionString;
			outputString += expression.getExpressionString() + " ";
			expression.getMessage() == "" ? "" :
				outputString += std::string("\n")			+ expression.getMessage();
			functionName == "" ? "" : 
				outputString += std::string("\nFunction:\t")	+ functionName + " ";
			groupName	 == "" ? "" :
				outputString += std::string("\nGroup:\t\t")		+ groupName + " ";
			outputString += "\n----------------------------------------\n";
			std::cout << outputString;
		}
	};
}