#pragma once
#include "BSharpBaseVisitor.h"


class Visitor : public BSharpBaseVisitor
{
public:
	virtual antlrcpp::Any visitProgram(BSharpParser::ProgramContext *ctx) override;
};

