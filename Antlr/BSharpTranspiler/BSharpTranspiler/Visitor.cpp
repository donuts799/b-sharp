#include "Visitor.h"

antlrcpp::Any Visitor::visitProgram(BSharpParser::ProgramContext *ctx)
{
	/*for (auto child : ctx->children)
	{
		std::cout << child->getText() << std::endl;
	}
	auto something = ctx->declaration_expression();
	for (BSharpParser::Declaration_expressionContext* i : something)
	{
		visitDeclaration_expression(i);
	}
	auto somethingElse = ctx->white_space();
	for (BSharpParser::White_spaceContext* i : somethingElse)
	{
		visitWhite_space(i);
	}*/
	antlrcpp::Any children = visitChildren(ctx);

	return children;
}
