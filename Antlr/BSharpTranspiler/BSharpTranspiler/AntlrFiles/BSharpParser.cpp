
// Generated from ..\\BSharp.g4 by ANTLR 4.8


#include "BSharpListener.h"
#include "BSharpVisitor.h"

#include "BSharpParser.h"


using namespace antlrcpp;
using namespace antlr4;

BSharpParser::BSharpParser(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

BSharpParser::~BSharpParser() {
  delete _interpreter;
}

std::string BSharpParser::getGrammarFileName() const {
  return "BSharp.g4";
}

const std::vector<std::string>& BSharpParser::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& BSharpParser::getVocabulary() const {
  return _vocabulary;
}


//----------------- ProgramContext ------------------------------------------------------------------

BSharpParser::ProgramContext::ProgramContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* BSharpParser::ProgramContext::EOF() {
  return getToken(BSharpParser::EOF, 0);
}

std::vector<BSharpParser::White_spaceContext *> BSharpParser::ProgramContext::white_space() {
  return getRuleContexts<BSharpParser::White_spaceContext>();
}

BSharpParser::White_spaceContext* BSharpParser::ProgramContext::white_space(size_t i) {
  return getRuleContext<BSharpParser::White_spaceContext>(i);
}

std::vector<BSharpParser::Declaration_expressionContext *> BSharpParser::ProgramContext::declaration_expression() {
  return getRuleContexts<BSharpParser::Declaration_expressionContext>();
}

BSharpParser::Declaration_expressionContext* BSharpParser::ProgramContext::declaration_expression(size_t i) {
  return getRuleContext<BSharpParser::Declaration_expressionContext>(i);
}


size_t BSharpParser::ProgramContext::getRuleIndex() const {
  return BSharpParser::RuleProgram;
}

void BSharpParser::ProgramContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterProgram(this);
}

void BSharpParser::ProgramContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitProgram(this);
}


antlrcpp::Any BSharpParser::ProgramContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitProgram(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::ProgramContext* BSharpParser::program() {
  ProgramContext *_localctx = _tracker.createInstance<ProgramContext>(_ctx, getState());
  enterRule(_localctx, 0, BSharpParser::RuleProgram);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(60); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(56);
      white_space();
      setState(57);
      declaration_expression();
      setState(58);
      white_space();
      setState(62); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << BSharpParser::LINE_COMMENT)
      | (1ULL << BSharpParser::MULTILINE_COMMENT)
      | (1ULL << BSharpParser::CLASS_TEXT)
      | (1ULL << BSharpParser::IMPORT_TEXT)
      | (1ULL << BSharpParser::LABEL)
      | (1ULL << BSharpParser::WHITE_SPACE))) != 0));
    setState(64);
    match(BSharpParser::EOF);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Declaration_expressionContext ------------------------------------------------------------------

BSharpParser::Declaration_expressionContext::Declaration_expressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

BSharpParser::Import_statementContext* BSharpParser::Declaration_expressionContext::import_statement() {
  return getRuleContext<BSharpParser::Import_statementContext>(0);
}

BSharpParser::Variable_declarationContext* BSharpParser::Declaration_expressionContext::variable_declaration() {
  return getRuleContext<BSharpParser::Variable_declarationContext>(0);
}

BSharpParser::Function_declarationContext* BSharpParser::Declaration_expressionContext::function_declaration() {
  return getRuleContext<BSharpParser::Function_declarationContext>(0);
}

BSharpParser::Structure_declarationContext* BSharpParser::Declaration_expressionContext::structure_declaration() {
  return getRuleContext<BSharpParser::Structure_declarationContext>(0);
}


size_t BSharpParser::Declaration_expressionContext::getRuleIndex() const {
  return BSharpParser::RuleDeclaration_expression;
}

void BSharpParser::Declaration_expressionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterDeclaration_expression(this);
}

void BSharpParser::Declaration_expressionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitDeclaration_expression(this);
}


antlrcpp::Any BSharpParser::Declaration_expressionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitDeclaration_expression(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::Declaration_expressionContext* BSharpParser::declaration_expression() {
  Declaration_expressionContext *_localctx = _tracker.createInstance<Declaration_expressionContext>(_ctx, getState());
  enterRule(_localctx, 2, BSharpParser::RuleDeclaration_expression);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(70);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 1, _ctx)) {
    case 1: {
      setState(66);
      import_statement();
      break;
    }

    case 2: {
      setState(67);
      variable_declaration();
      break;
    }

    case 3: {
      setState(68);
      function_declaration();
      break;
    }

    case 4: {
      setState(69);
      structure_declaration();
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Import_statementContext ------------------------------------------------------------------

BSharpParser::Import_statementContext::Import_statementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* BSharpParser::Import_statementContext::IMPORT_TEXT() {
  return getToken(BSharpParser::IMPORT_TEXT, 0);
}

BSharpParser::White_space1Context* BSharpParser::Import_statementContext::white_space1() {
  return getRuleContext<BSharpParser::White_space1Context>(0);
}

BSharpParser::StringContext* BSharpParser::Import_statementContext::string() {
  return getRuleContext<BSharpParser::StringContext>(0);
}

BSharpParser::White_spaceContext* BSharpParser::Import_statementContext::white_space() {
  return getRuleContext<BSharpParser::White_spaceContext>(0);
}

tree::TerminalNode* BSharpParser::Import_statementContext::SEMICOLON() {
  return getToken(BSharpParser::SEMICOLON, 0);
}


size_t BSharpParser::Import_statementContext::getRuleIndex() const {
  return BSharpParser::RuleImport_statement;
}

void BSharpParser::Import_statementContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterImport_statement(this);
}

void BSharpParser::Import_statementContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitImport_statement(this);
}


antlrcpp::Any BSharpParser::Import_statementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitImport_statement(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::Import_statementContext* BSharpParser::import_statement() {
  Import_statementContext *_localctx = _tracker.createInstance<Import_statementContext>(_ctx, getState());
  enterRule(_localctx, 4, BSharpParser::RuleImport_statement);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(72);
    match(BSharpParser::IMPORT_TEXT);
    setState(73);
    white_space1();
    setState(74);
    string();
    setState(75);
    white_space();
    setState(76);
    match(BSharpParser::SEMICOLON);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Function_declarationContext ------------------------------------------------------------------

BSharpParser::Function_declarationContext::Function_declarationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

BSharpParser::Function_headingContext* BSharpParser::Function_declarationContext::function_heading() {
  return getRuleContext<BSharpParser::Function_headingContext>(0);
}

BSharpParser::White_spaceContext* BSharpParser::Function_declarationContext::white_space() {
  return getRuleContext<BSharpParser::White_spaceContext>(0);
}

BSharpParser::Function_bodyContext* BSharpParser::Function_declarationContext::function_body() {
  return getRuleContext<BSharpParser::Function_bodyContext>(0);
}


size_t BSharpParser::Function_declarationContext::getRuleIndex() const {
  return BSharpParser::RuleFunction_declaration;
}

void BSharpParser::Function_declarationContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFunction_declaration(this);
}

void BSharpParser::Function_declarationContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFunction_declaration(this);
}


antlrcpp::Any BSharpParser::Function_declarationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitFunction_declaration(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::Function_declarationContext* BSharpParser::function_declaration() {
  Function_declarationContext *_localctx = _tracker.createInstance<Function_declarationContext>(_ctx, getState());
  enterRule(_localctx, 6, BSharpParser::RuleFunction_declaration);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(78);
    function_heading();
    setState(79);
    white_space();
    setState(80);
    function_body();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Variable_declarationContext ------------------------------------------------------------------

BSharpParser::Variable_declarationContext::Variable_declarationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

BSharpParser::Variable_nameContext* BSharpParser::Variable_declarationContext::variable_name() {
  return getRuleContext<BSharpParser::Variable_nameContext>(0);
}

std::vector<BSharpParser::White_spaceContext *> BSharpParser::Variable_declarationContext::white_space() {
  return getRuleContexts<BSharpParser::White_spaceContext>();
}

BSharpParser::White_spaceContext* BSharpParser::Variable_declarationContext::white_space(size_t i) {
  return getRuleContext<BSharpParser::White_spaceContext>(i);
}

tree::TerminalNode* BSharpParser::Variable_declarationContext::SEMICOLON() {
  return getToken(BSharpParser::SEMICOLON, 0);
}

tree::TerminalNode* BSharpParser::Variable_declarationContext::EQUAL() {
  return getToken(BSharpParser::EQUAL, 0);
}

BSharpParser::Valued_expressionContext* BSharpParser::Variable_declarationContext::valued_expression() {
  return getRuleContext<BSharpParser::Valued_expressionContext>(0);
}


size_t BSharpParser::Variable_declarationContext::getRuleIndex() const {
  return BSharpParser::RuleVariable_declaration;
}

void BSharpParser::Variable_declarationContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterVariable_declaration(this);
}

void BSharpParser::Variable_declarationContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitVariable_declaration(this);
}


antlrcpp::Any BSharpParser::Variable_declarationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitVariable_declaration(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::Variable_declarationContext* BSharpParser::variable_declaration() {
  Variable_declarationContext *_localctx = _tracker.createInstance<Variable_declarationContext>(_ctx, getState());
  enterRule(_localctx, 8, BSharpParser::RuleVariable_declaration);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(82);
    variable_name();
    setState(88);
    _errHandler->sync(this);

    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 2, _ctx)) {
    case 1: {
      setState(83);
      white_space();
      setState(84);
      match(BSharpParser::EQUAL);
      setState(85);
      white_space();
      setState(86);
      valued_expression();
      break;
    }

    }
    setState(90);
    white_space();
    setState(91);
    match(BSharpParser::SEMICOLON);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Structure_declarationContext ------------------------------------------------------------------

BSharpParser::Structure_declarationContext::Structure_declarationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

BSharpParser::Class_declarationContext* BSharpParser::Structure_declarationContext::class_declaration() {
  return getRuleContext<BSharpParser::Class_declarationContext>(0);
}


size_t BSharpParser::Structure_declarationContext::getRuleIndex() const {
  return BSharpParser::RuleStructure_declaration;
}

void BSharpParser::Structure_declarationContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterStructure_declaration(this);
}

void BSharpParser::Structure_declarationContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitStructure_declaration(this);
}


antlrcpp::Any BSharpParser::Structure_declarationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitStructure_declaration(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::Structure_declarationContext* BSharpParser::structure_declaration() {
  Structure_declarationContext *_localctx = _tracker.createInstance<Structure_declarationContext>(_ctx, getState());
  enterRule(_localctx, 10, BSharpParser::RuleStructure_declaration);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(93);
    class_declaration();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Class_declarationContext ------------------------------------------------------------------

BSharpParser::Class_declarationContext::Class_declarationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

BSharpParser::Class_headerContext* BSharpParser::Class_declarationContext::class_header() {
  return getRuleContext<BSharpParser::Class_headerContext>(0);
}

BSharpParser::White_spaceContext* BSharpParser::Class_declarationContext::white_space() {
  return getRuleContext<BSharpParser::White_spaceContext>(0);
}

BSharpParser::Class_bodyContext* BSharpParser::Class_declarationContext::class_body() {
  return getRuleContext<BSharpParser::Class_bodyContext>(0);
}


size_t BSharpParser::Class_declarationContext::getRuleIndex() const {
  return BSharpParser::RuleClass_declaration;
}

void BSharpParser::Class_declarationContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterClass_declaration(this);
}

void BSharpParser::Class_declarationContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitClass_declaration(this);
}


antlrcpp::Any BSharpParser::Class_declarationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitClass_declaration(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::Class_declarationContext* BSharpParser::class_declaration() {
  Class_declarationContext *_localctx = _tracker.createInstance<Class_declarationContext>(_ctx, getState());
  enterRule(_localctx, 12, BSharpParser::RuleClass_declaration);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(95);
    class_header();
    setState(96);
    white_space();
    setState(97);
    class_body();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Class_headerContext ------------------------------------------------------------------

BSharpParser::Class_headerContext::Class_headerContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* BSharpParser::Class_headerContext::CLASS_TEXT() {
  return getToken(BSharpParser::CLASS_TEXT, 0);
}

BSharpParser::White_spaceContext* BSharpParser::Class_headerContext::white_space() {
  return getRuleContext<BSharpParser::White_spaceContext>(0);
}

BSharpParser::Class_nameContext* BSharpParser::Class_headerContext::class_name() {
  return getRuleContext<BSharpParser::Class_nameContext>(0);
}


size_t BSharpParser::Class_headerContext::getRuleIndex() const {
  return BSharpParser::RuleClass_header;
}

void BSharpParser::Class_headerContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterClass_header(this);
}

void BSharpParser::Class_headerContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitClass_header(this);
}


antlrcpp::Any BSharpParser::Class_headerContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitClass_header(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::Class_headerContext* BSharpParser::class_header() {
  Class_headerContext *_localctx = _tracker.createInstance<Class_headerContext>(_ctx, getState());
  enterRule(_localctx, 14, BSharpParser::RuleClass_header);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(99);
    match(BSharpParser::CLASS_TEXT);
    setState(100);
    white_space();
    setState(101);
    class_name();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Class_nameContext ------------------------------------------------------------------

BSharpParser::Class_nameContext::Class_nameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* BSharpParser::Class_nameContext::LABEL() {
  return getToken(BSharpParser::LABEL, 0);
}


size_t BSharpParser::Class_nameContext::getRuleIndex() const {
  return BSharpParser::RuleClass_name;
}

void BSharpParser::Class_nameContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterClass_name(this);
}

void BSharpParser::Class_nameContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitClass_name(this);
}


antlrcpp::Any BSharpParser::Class_nameContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitClass_name(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::Class_nameContext* BSharpParser::class_name() {
  Class_nameContext *_localctx = _tracker.createInstance<Class_nameContext>(_ctx, getState());
  enterRule(_localctx, 16, BSharpParser::RuleClass_name);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(103);
    match(BSharpParser::LABEL);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Class_bodyContext ------------------------------------------------------------------

BSharpParser::Class_bodyContext::Class_bodyContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* BSharpParser::Class_bodyContext::OPEN_BRACKET() {
  return getToken(BSharpParser::OPEN_BRACKET, 0);
}

std::vector<BSharpParser::White_spaceContext *> BSharpParser::Class_bodyContext::white_space() {
  return getRuleContexts<BSharpParser::White_spaceContext>();
}

BSharpParser::White_spaceContext* BSharpParser::Class_bodyContext::white_space(size_t i) {
  return getRuleContext<BSharpParser::White_spaceContext>(i);
}

tree::TerminalNode* BSharpParser::Class_bodyContext::CLOSE_BRACKET() {
  return getToken(BSharpParser::CLOSE_BRACKET, 0);
}

std::vector<BSharpParser::Expression_classContext *> BSharpParser::Class_bodyContext::expression_class() {
  return getRuleContexts<BSharpParser::Expression_classContext>();
}

BSharpParser::Expression_classContext* BSharpParser::Class_bodyContext::expression_class(size_t i) {
  return getRuleContext<BSharpParser::Expression_classContext>(i);
}


size_t BSharpParser::Class_bodyContext::getRuleIndex() const {
  return BSharpParser::RuleClass_body;
}

void BSharpParser::Class_bodyContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterClass_body(this);
}

void BSharpParser::Class_bodyContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitClass_body(this);
}


antlrcpp::Any BSharpParser::Class_bodyContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitClass_body(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::Class_bodyContext* BSharpParser::class_body() {
  Class_bodyContext *_localctx = _tracker.createInstance<Class_bodyContext>(_ctx, getState());
  enterRule(_localctx, 18, BSharpParser::RuleClass_body);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(105);
    match(BSharpParser::OPEN_BRACKET);
    setState(106);
    white_space();
    setState(112);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << BSharpParser::CLASS_TEXT)
      | (1ULL << BSharpParser::IMPORT_TEXT)
      | (1ULL << BSharpParser::LABEL))) != 0)) {
      setState(107);
      expression_class();
      setState(108);
      white_space();
      setState(114);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(115);
    match(BSharpParser::CLOSE_BRACKET);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Expression_classContext ------------------------------------------------------------------

BSharpParser::Expression_classContext::Expression_classContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

BSharpParser::Declaration_expressionContext* BSharpParser::Expression_classContext::declaration_expression() {
  return getRuleContext<BSharpParser::Declaration_expressionContext>(0);
}


size_t BSharpParser::Expression_classContext::getRuleIndex() const {
  return BSharpParser::RuleExpression_class;
}

void BSharpParser::Expression_classContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterExpression_class(this);
}

void BSharpParser::Expression_classContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitExpression_class(this);
}


antlrcpp::Any BSharpParser::Expression_classContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitExpression_class(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::Expression_classContext* BSharpParser::expression_class() {
  Expression_classContext *_localctx = _tracker.createInstance<Expression_classContext>(_ctx, getState());
  enterRule(_localctx, 20, BSharpParser::RuleExpression_class);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(117);
    declaration_expression();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Function_headingContext ------------------------------------------------------------------

BSharpParser::Function_headingContext::Function_headingContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

BSharpParser::Type_nameContext* BSharpParser::Function_headingContext::type_name() {
  return getRuleContext<BSharpParser::Type_nameContext>(0);
}

BSharpParser::White_space1Context* BSharpParser::Function_headingContext::white_space1() {
  return getRuleContext<BSharpParser::White_space1Context>(0);
}

BSharpParser::Function_nameContext* BSharpParser::Function_headingContext::function_name() {
  return getRuleContext<BSharpParser::Function_nameContext>(0);
}

std::vector<BSharpParser::White_spaceContext *> BSharpParser::Function_headingContext::white_space() {
  return getRuleContexts<BSharpParser::White_spaceContext>();
}

BSharpParser::White_spaceContext* BSharpParser::Function_headingContext::white_space(size_t i) {
  return getRuleContext<BSharpParser::White_spaceContext>(i);
}

tree::TerminalNode* BSharpParser::Function_headingContext::OPEN_PAREN() {
  return getToken(BSharpParser::OPEN_PAREN, 0);
}

tree::TerminalNode* BSharpParser::Function_headingContext::CLOSE_PAREN() {
  return getToken(BSharpParser::CLOSE_PAREN, 0);
}

BSharpParser::Function_parametersContext* BSharpParser::Function_headingContext::function_parameters() {
  return getRuleContext<BSharpParser::Function_parametersContext>(0);
}


size_t BSharpParser::Function_headingContext::getRuleIndex() const {
  return BSharpParser::RuleFunction_heading;
}

void BSharpParser::Function_headingContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFunction_heading(this);
}

void BSharpParser::Function_headingContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFunction_heading(this);
}


antlrcpp::Any BSharpParser::Function_headingContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitFunction_heading(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::Function_headingContext* BSharpParser::function_heading() {
  Function_headingContext *_localctx = _tracker.createInstance<Function_headingContext>(_ctx, getState());
  enterRule(_localctx, 22, BSharpParser::RuleFunction_heading);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(119);
    type_name();
    setState(120);
    white_space1();
    setState(121);
    function_name();
    setState(122);
    white_space();
    setState(123);
    match(BSharpParser::OPEN_PAREN);
    setState(124);
    white_space();
    setState(128);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == BSharpParser::LABEL) {
      setState(125);
      function_parameters();
      setState(126);
      white_space();
    }
    setState(130);
    match(BSharpParser::CLOSE_PAREN);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Function_parametersContext ------------------------------------------------------------------

BSharpParser::Function_parametersContext::Function_parametersContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<BSharpParser::Function_parameterContext *> BSharpParser::Function_parametersContext::function_parameter() {
  return getRuleContexts<BSharpParser::Function_parameterContext>();
}

BSharpParser::Function_parameterContext* BSharpParser::Function_parametersContext::function_parameter(size_t i) {
  return getRuleContext<BSharpParser::Function_parameterContext>(i);
}

std::vector<BSharpParser::White_spaceContext *> BSharpParser::Function_parametersContext::white_space() {
  return getRuleContexts<BSharpParser::White_spaceContext>();
}

BSharpParser::White_spaceContext* BSharpParser::Function_parametersContext::white_space(size_t i) {
  return getRuleContext<BSharpParser::White_spaceContext>(i);
}

std::vector<tree::TerminalNode *> BSharpParser::Function_parametersContext::COMMA() {
  return getTokens(BSharpParser::COMMA);
}

tree::TerminalNode* BSharpParser::Function_parametersContext::COMMA(size_t i) {
  return getToken(BSharpParser::COMMA, i);
}


size_t BSharpParser::Function_parametersContext::getRuleIndex() const {
  return BSharpParser::RuleFunction_parameters;
}

void BSharpParser::Function_parametersContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFunction_parameters(this);
}

void BSharpParser::Function_parametersContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFunction_parameters(this);
}


antlrcpp::Any BSharpParser::Function_parametersContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitFunction_parameters(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::Function_parametersContext* BSharpParser::function_parameters() {
  Function_parametersContext *_localctx = _tracker.createInstance<Function_parametersContext>(_ctx, getState());
  enterRule(_localctx, 24, BSharpParser::RuleFunction_parameters);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(132);
    function_parameter();
    setState(140);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 5, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(133);
        white_space();
        setState(134);
        match(BSharpParser::COMMA);
        setState(135);
        white_space();
        setState(136);
        function_parameter(); 
      }
      setState(142);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 5, _ctx);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Function_parameterContext ------------------------------------------------------------------

BSharpParser::Function_parameterContext::Function_parameterContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

BSharpParser::Type_nameContext* BSharpParser::Function_parameterContext::type_name() {
  return getRuleContext<BSharpParser::Type_nameContext>(0);
}

BSharpParser::White_space1Context* BSharpParser::Function_parameterContext::white_space1() {
  return getRuleContext<BSharpParser::White_space1Context>(0);
}

BSharpParser::Parameter_nameContext* BSharpParser::Function_parameterContext::parameter_name() {
  return getRuleContext<BSharpParser::Parameter_nameContext>(0);
}

std::vector<BSharpParser::White_spaceContext *> BSharpParser::Function_parameterContext::white_space() {
  return getRuleContexts<BSharpParser::White_spaceContext>();
}

BSharpParser::White_spaceContext* BSharpParser::Function_parameterContext::white_space(size_t i) {
  return getRuleContext<BSharpParser::White_spaceContext>(i);
}

tree::TerminalNode* BSharpParser::Function_parameterContext::EQUAL() {
  return getToken(BSharpParser::EQUAL, 0);
}

BSharpParser::Valued_expressionContext* BSharpParser::Function_parameterContext::valued_expression() {
  return getRuleContext<BSharpParser::Valued_expressionContext>(0);
}


size_t BSharpParser::Function_parameterContext::getRuleIndex() const {
  return BSharpParser::RuleFunction_parameter;
}

void BSharpParser::Function_parameterContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFunction_parameter(this);
}

void BSharpParser::Function_parameterContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFunction_parameter(this);
}


antlrcpp::Any BSharpParser::Function_parameterContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitFunction_parameter(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::Function_parameterContext* BSharpParser::function_parameter() {
  Function_parameterContext *_localctx = _tracker.createInstance<Function_parameterContext>(_ctx, getState());
  enterRule(_localctx, 26, BSharpParser::RuleFunction_parameter);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(143);
    type_name();
    setState(144);
    white_space1();
    setState(145);
    parameter_name();
    setState(151);
    _errHandler->sync(this);

    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 6, _ctx)) {
    case 1: {
      setState(146);
      white_space();
      setState(147);
      match(BSharpParser::EQUAL);
      setState(148);
      white_space();
      setState(149);
      valued_expression();
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Function_bodyContext ------------------------------------------------------------------

BSharpParser::Function_bodyContext::Function_bodyContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* BSharpParser::Function_bodyContext::OPEN_BRACKET() {
  return getToken(BSharpParser::OPEN_BRACKET, 0);
}

std::vector<BSharpParser::White_spaceContext *> BSharpParser::Function_bodyContext::white_space() {
  return getRuleContexts<BSharpParser::White_spaceContext>();
}

BSharpParser::White_spaceContext* BSharpParser::Function_bodyContext::white_space(size_t i) {
  return getRuleContext<BSharpParser::White_spaceContext>(i);
}

tree::TerminalNode* BSharpParser::Function_bodyContext::CLOSE_BRACKET() {
  return getToken(BSharpParser::CLOSE_BRACKET, 0);
}

std::vector<BSharpParser::Function_expressionContext *> BSharpParser::Function_bodyContext::function_expression() {
  return getRuleContexts<BSharpParser::Function_expressionContext>();
}

BSharpParser::Function_expressionContext* BSharpParser::Function_bodyContext::function_expression(size_t i) {
  return getRuleContext<BSharpParser::Function_expressionContext>(i);
}

std::vector<tree::TerminalNode *> BSharpParser::Function_bodyContext::SEMICOLON() {
  return getTokens(BSharpParser::SEMICOLON);
}

tree::TerminalNode* BSharpParser::Function_bodyContext::SEMICOLON(size_t i) {
  return getToken(BSharpParser::SEMICOLON, i);
}

BSharpParser::Return_statementContext* BSharpParser::Function_bodyContext::return_statement() {
  return getRuleContext<BSharpParser::Return_statementContext>(0);
}


size_t BSharpParser::Function_bodyContext::getRuleIndex() const {
  return BSharpParser::RuleFunction_body;
}

void BSharpParser::Function_bodyContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFunction_body(this);
}

void BSharpParser::Function_bodyContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFunction_body(this);
}


antlrcpp::Any BSharpParser::Function_bodyContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitFunction_body(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::Function_bodyContext* BSharpParser::function_body() {
  Function_bodyContext *_localctx = _tracker.createInstance<Function_bodyContext>(_ctx, getState());
  enterRule(_localctx, 28, BSharpParser::RuleFunction_body);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(153);
    match(BSharpParser::OPEN_BRACKET);
    setState(154);
    white_space();
    setState(162);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << BSharpParser::CLASS_TEXT)
      | (1ULL << BSharpParser::IMPORT_TEXT)
      | (1ULL << BSharpParser::LABEL))) != 0)) {
      setState(155);
      function_expression();
      setState(156);
      white_space();
      setState(157);
      match(BSharpParser::SEMICOLON);
      setState(158);
      white_space();
      setState(164);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(166);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == BSharpParser::RETURN_TEXT) {
      setState(165);
      return_statement();
    }
    setState(168);
    white_space();
    setState(169);
    match(BSharpParser::CLOSE_BRACKET);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Parameter_nameContext ------------------------------------------------------------------

BSharpParser::Parameter_nameContext::Parameter_nameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* BSharpParser::Parameter_nameContext::LABEL() {
  return getToken(BSharpParser::LABEL, 0);
}


size_t BSharpParser::Parameter_nameContext::getRuleIndex() const {
  return BSharpParser::RuleParameter_name;
}

void BSharpParser::Parameter_nameContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterParameter_name(this);
}

void BSharpParser::Parameter_nameContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitParameter_name(this);
}


antlrcpp::Any BSharpParser::Parameter_nameContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitParameter_name(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::Parameter_nameContext* BSharpParser::parameter_name() {
  Parameter_nameContext *_localctx = _tracker.createInstance<Parameter_nameContext>(_ctx, getState());
  enterRule(_localctx, 30, BSharpParser::RuleParameter_name);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(171);
    match(BSharpParser::LABEL);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Return_statementContext ------------------------------------------------------------------

BSharpParser::Return_statementContext::Return_statementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* BSharpParser::Return_statementContext::RETURN_TEXT() {
  return getToken(BSharpParser::RETURN_TEXT, 0);
}

BSharpParser::White_spaceContext* BSharpParser::Return_statementContext::white_space() {
  return getRuleContext<BSharpParser::White_spaceContext>(0);
}

tree::TerminalNode* BSharpParser::Return_statementContext::SEMICOLON() {
  return getToken(BSharpParser::SEMICOLON, 0);
}

BSharpParser::White_space1Context* BSharpParser::Return_statementContext::white_space1() {
  return getRuleContext<BSharpParser::White_space1Context>(0);
}

BSharpParser::Valued_expressionContext* BSharpParser::Return_statementContext::valued_expression() {
  return getRuleContext<BSharpParser::Valued_expressionContext>(0);
}


size_t BSharpParser::Return_statementContext::getRuleIndex() const {
  return BSharpParser::RuleReturn_statement;
}

void BSharpParser::Return_statementContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterReturn_statement(this);
}

void BSharpParser::Return_statementContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitReturn_statement(this);
}


antlrcpp::Any BSharpParser::Return_statementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitReturn_statement(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::Return_statementContext* BSharpParser::return_statement() {
  Return_statementContext *_localctx = _tracker.createInstance<Return_statementContext>(_ctx, getState());
  enterRule(_localctx, 32, BSharpParser::RuleReturn_statement);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(173);
    match(BSharpParser::RETURN_TEXT);
    setState(177);
    _errHandler->sync(this);

    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 9, _ctx)) {
    case 1: {
      setState(174);
      white_space1();
      setState(175);
      valued_expression();
      break;
    }

    }
    setState(179);
    white_space();
    setState(180);
    match(BSharpParser::SEMICOLON);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Function_nameContext ------------------------------------------------------------------

BSharpParser::Function_nameContext::Function_nameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* BSharpParser::Function_nameContext::LABEL() {
  return getToken(BSharpParser::LABEL, 0);
}


size_t BSharpParser::Function_nameContext::getRuleIndex() const {
  return BSharpParser::RuleFunction_name;
}

void BSharpParser::Function_nameContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFunction_name(this);
}

void BSharpParser::Function_nameContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFunction_name(this);
}


antlrcpp::Any BSharpParser::Function_nameContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitFunction_name(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::Function_nameContext* BSharpParser::function_name() {
  Function_nameContext *_localctx = _tracker.createInstance<Function_nameContext>(_ctx, getState());
  enterRule(_localctx, 34, BSharpParser::RuleFunction_name);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(182);
    match(BSharpParser::LABEL);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Function_expressionContext ------------------------------------------------------------------

BSharpParser::Function_expressionContext::Function_expressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

BSharpParser::Declaration_expressionContext* BSharpParser::Function_expressionContext::declaration_expression() {
  return getRuleContext<BSharpParser::Declaration_expressionContext>(0);
}

BSharpParser::Valued_expressionContext* BSharpParser::Function_expressionContext::valued_expression() {
  return getRuleContext<BSharpParser::Valued_expressionContext>(0);
}

BSharpParser::AssignmentContext* BSharpParser::Function_expressionContext::assignment() {
  return getRuleContext<BSharpParser::AssignmentContext>(0);
}


size_t BSharpParser::Function_expressionContext::getRuleIndex() const {
  return BSharpParser::RuleFunction_expression;
}

void BSharpParser::Function_expressionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFunction_expression(this);
}

void BSharpParser::Function_expressionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFunction_expression(this);
}


antlrcpp::Any BSharpParser::Function_expressionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitFunction_expression(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::Function_expressionContext* BSharpParser::function_expression() {
  Function_expressionContext *_localctx = _tracker.createInstance<Function_expressionContext>(_ctx, getState());
  enterRule(_localctx, 36, BSharpParser::RuleFunction_expression);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(187);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 10, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(184);
      declaration_expression();
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(185);
      valued_expression();
      break;
    }

    case 3: {
      enterOuterAlt(_localctx, 3);
      setState(186);
      assignment();
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Function_callContext ------------------------------------------------------------------

BSharpParser::Function_callContext::Function_callContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

BSharpParser::Function_nameContext* BSharpParser::Function_callContext::function_name() {
  return getRuleContext<BSharpParser::Function_nameContext>(0);
}

std::vector<BSharpParser::White_spaceContext *> BSharpParser::Function_callContext::white_space() {
  return getRuleContexts<BSharpParser::White_spaceContext>();
}

BSharpParser::White_spaceContext* BSharpParser::Function_callContext::white_space(size_t i) {
  return getRuleContext<BSharpParser::White_spaceContext>(i);
}

tree::TerminalNode* BSharpParser::Function_callContext::OPEN_PAREN() {
  return getToken(BSharpParser::OPEN_PAREN, 0);
}

tree::TerminalNode* BSharpParser::Function_callContext::CLOSE_PAREN() {
  return getToken(BSharpParser::CLOSE_PAREN, 0);
}

std::vector<BSharpParser::Valued_expressionContext *> BSharpParser::Function_callContext::valued_expression() {
  return getRuleContexts<BSharpParser::Valued_expressionContext>();
}

BSharpParser::Valued_expressionContext* BSharpParser::Function_callContext::valued_expression(size_t i) {
  return getRuleContext<BSharpParser::Valued_expressionContext>(i);
}

std::vector<tree::TerminalNode *> BSharpParser::Function_callContext::COMMA() {
  return getTokens(BSharpParser::COMMA);
}

tree::TerminalNode* BSharpParser::Function_callContext::COMMA(size_t i) {
  return getToken(BSharpParser::COMMA, i);
}


size_t BSharpParser::Function_callContext::getRuleIndex() const {
  return BSharpParser::RuleFunction_call;
}

void BSharpParser::Function_callContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFunction_call(this);
}

void BSharpParser::Function_callContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFunction_call(this);
}


antlrcpp::Any BSharpParser::Function_callContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitFunction_call(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::Function_callContext* BSharpParser::function_call() {
  Function_callContext *_localctx = _tracker.createInstance<Function_callContext>(_ctx, getState());
  enterRule(_localctx, 38, BSharpParser::RuleFunction_call);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(189);
    function_name();
    setState(190);
    white_space();
    setState(191);
    match(BSharpParser::OPEN_PAREN);
    setState(206);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << BSharpParser::LINE_COMMENT)
      | (1ULL << BSharpParser::MULTILINE_COMMENT)
      | (1ULL << BSharpParser::LABEL)
      | (1ULL << BSharpParser::WHITE_SPACE))) != 0)) {
      setState(192);
      white_space();
      setState(193);
      valued_expression();
      setState(201);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 11, _ctx);
      while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
        if (alt == 1) {
          setState(194);
          white_space();
          setState(195);
          match(BSharpParser::COMMA);
          setState(196);
          white_space();
          setState(197);
          valued_expression(); 
        }
        setState(203);
        _errHandler->sync(this);
        alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 11, _ctx);
      }
      setState(204);
      white_space();
    }
    setState(208);
    match(BSharpParser::CLOSE_PAREN);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Valued_expressionContext ------------------------------------------------------------------

BSharpParser::Valued_expressionContext::Valued_expressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

BSharpParser::Function_callContext* BSharpParser::Valued_expressionContext::function_call() {
  return getRuleContext<BSharpParser::Function_callContext>(0);
}


size_t BSharpParser::Valued_expressionContext::getRuleIndex() const {
  return BSharpParser::RuleValued_expression;
}

void BSharpParser::Valued_expressionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterValued_expression(this);
}

void BSharpParser::Valued_expressionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitValued_expression(this);
}


antlrcpp::Any BSharpParser::Valued_expressionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitValued_expression(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::Valued_expressionContext* BSharpParser::valued_expression() {
  Valued_expressionContext *_localctx = _tracker.createInstance<Valued_expressionContext>(_ctx, getState());
  enterRule(_localctx, 40, BSharpParser::RuleValued_expression);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(210);
    function_call();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Type_nameContext ------------------------------------------------------------------

BSharpParser::Type_nameContext::Type_nameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* BSharpParser::Type_nameContext::LABEL() {
  return getToken(BSharpParser::LABEL, 0);
}


size_t BSharpParser::Type_nameContext::getRuleIndex() const {
  return BSharpParser::RuleType_name;
}

void BSharpParser::Type_nameContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterType_name(this);
}

void BSharpParser::Type_nameContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitType_name(this);
}


antlrcpp::Any BSharpParser::Type_nameContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitType_name(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::Type_nameContext* BSharpParser::type_name() {
  Type_nameContext *_localctx = _tracker.createInstance<Type_nameContext>(_ctx, getState());
  enterRule(_localctx, 42, BSharpParser::RuleType_name);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(212);
    match(BSharpParser::LABEL);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AssignmentContext ------------------------------------------------------------------

BSharpParser::AssignmentContext::AssignmentContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

BSharpParser::Variable_nameContext* BSharpParser::AssignmentContext::variable_name() {
  return getRuleContext<BSharpParser::Variable_nameContext>(0);
}

BSharpParser::White_space1Context* BSharpParser::AssignmentContext::white_space1() {
  return getRuleContext<BSharpParser::White_space1Context>(0);
}

tree::TerminalNode* BSharpParser::AssignmentContext::EQUAL() {
  return getToken(BSharpParser::EQUAL, 0);
}

BSharpParser::Valued_expressionContext* BSharpParser::AssignmentContext::valued_expression() {
  return getRuleContext<BSharpParser::Valued_expressionContext>(0);
}

BSharpParser::White_spaceContext* BSharpParser::AssignmentContext::white_space() {
  return getRuleContext<BSharpParser::White_spaceContext>(0);
}

tree::TerminalNode* BSharpParser::AssignmentContext::SEMICOLON() {
  return getToken(BSharpParser::SEMICOLON, 0);
}


size_t BSharpParser::AssignmentContext::getRuleIndex() const {
  return BSharpParser::RuleAssignment;
}

void BSharpParser::AssignmentContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterAssignment(this);
}

void BSharpParser::AssignmentContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitAssignment(this);
}


antlrcpp::Any BSharpParser::AssignmentContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitAssignment(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::AssignmentContext* BSharpParser::assignment() {
  AssignmentContext *_localctx = _tracker.createInstance<AssignmentContext>(_ctx, getState());
  enterRule(_localctx, 44, BSharpParser::RuleAssignment);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(214);
    variable_name();
    setState(215);
    white_space1();
    setState(216);
    match(BSharpParser::EQUAL);
    setState(217);
    valued_expression();
    setState(218);
    white_space();
    setState(219);
    match(BSharpParser::SEMICOLON);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Variable_nameContext ------------------------------------------------------------------

BSharpParser::Variable_nameContext::Variable_nameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* BSharpParser::Variable_nameContext::LABEL() {
  return getToken(BSharpParser::LABEL, 0);
}


size_t BSharpParser::Variable_nameContext::getRuleIndex() const {
  return BSharpParser::RuleVariable_name;
}

void BSharpParser::Variable_nameContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterVariable_name(this);
}

void BSharpParser::Variable_nameContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitVariable_name(this);
}


antlrcpp::Any BSharpParser::Variable_nameContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitVariable_name(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::Variable_nameContext* BSharpParser::variable_name() {
  Variable_nameContext *_localctx = _tracker.createInstance<Variable_nameContext>(_ctx, getState());
  enterRule(_localctx, 46, BSharpParser::RuleVariable_name);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(221);
    match(BSharpParser::LABEL);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StringContext ------------------------------------------------------------------

BSharpParser::StringContext::StringContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* BSharpParser::StringContext::STRING() {
  return getToken(BSharpParser::STRING, 0);
}


size_t BSharpParser::StringContext::getRuleIndex() const {
  return BSharpParser::RuleString;
}

void BSharpParser::StringContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterString(this);
}

void BSharpParser::StringContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitString(this);
}


antlrcpp::Any BSharpParser::StringContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitString(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::StringContext* BSharpParser::string() {
  StringContext *_localctx = _tracker.createInstance<StringContext>(_ctx, getState());
  enterRule(_localctx, 48, BSharpParser::RuleString);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(223);
    match(BSharpParser::STRING);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- White_space1Context ------------------------------------------------------------------

BSharpParser::White_space1Context::White_space1Context(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<tree::TerminalNode *> BSharpParser::White_space1Context::WHITE_SPACE() {
  return getTokens(BSharpParser::WHITE_SPACE);
}

tree::TerminalNode* BSharpParser::White_space1Context::WHITE_SPACE(size_t i) {
  return getToken(BSharpParser::WHITE_SPACE, i);
}

std::vector<BSharpParser::CommentContext *> BSharpParser::White_space1Context::comment() {
  return getRuleContexts<BSharpParser::CommentContext>();
}

BSharpParser::CommentContext* BSharpParser::White_space1Context::comment(size_t i) {
  return getRuleContext<BSharpParser::CommentContext>(i);
}


size_t BSharpParser::White_space1Context::getRuleIndex() const {
  return BSharpParser::RuleWhite_space1;
}

void BSharpParser::White_space1Context::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterWhite_space1(this);
}

void BSharpParser::White_space1Context::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitWhite_space1(this);
}


antlrcpp::Any BSharpParser::White_space1Context::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitWhite_space1(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::White_space1Context* BSharpParser::white_space1() {
  White_space1Context *_localctx = _tracker.createInstance<White_space1Context>(_ctx, getState());
  enterRule(_localctx, 50, BSharpParser::RuleWhite_space1);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(238); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(228);
      _errHandler->sync(this);
      _la = _input->LA(1);
      while (_la == BSharpParser::LINE_COMMENT

      || _la == BSharpParser::MULTILINE_COMMENT) {
        setState(225);
        comment();
        setState(230);
        _errHandler->sync(this);
        _la = _input->LA(1);
      }
      setState(231);
      match(BSharpParser::WHITE_SPACE);

      setState(235);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 14, _ctx);
      while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
        if (alt == 1) {
          setState(232);
          comment(); 
        }
        setState(237);
        _errHandler->sync(this);
        alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 14, _ctx);
      }
      setState(240); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << BSharpParser::LINE_COMMENT)
      | (1ULL << BSharpParser::MULTILINE_COMMENT)
      | (1ULL << BSharpParser::WHITE_SPACE))) != 0));
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- White_spaceContext ------------------------------------------------------------------

BSharpParser::White_spaceContext::White_spaceContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<BSharpParser::CommentContext *> BSharpParser::White_spaceContext::comment() {
  return getRuleContexts<BSharpParser::CommentContext>();
}

BSharpParser::CommentContext* BSharpParser::White_spaceContext::comment(size_t i) {
  return getRuleContext<BSharpParser::CommentContext>(i);
}

std::vector<tree::TerminalNode *> BSharpParser::White_spaceContext::MULTILINE_COMMENT() {
  return getTokens(BSharpParser::MULTILINE_COMMENT);
}

tree::TerminalNode* BSharpParser::White_spaceContext::MULTILINE_COMMENT(size_t i) {
  return getToken(BSharpParser::MULTILINE_COMMENT, i);
}

std::vector<tree::TerminalNode *> BSharpParser::White_spaceContext::WHITE_SPACE() {
  return getTokens(BSharpParser::WHITE_SPACE);
}

tree::TerminalNode* BSharpParser::White_spaceContext::WHITE_SPACE(size_t i) {
  return getToken(BSharpParser::WHITE_SPACE, i);
}


size_t BSharpParser::White_spaceContext::getRuleIndex() const {
  return BSharpParser::RuleWhite_space;
}

void BSharpParser::White_spaceContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterWhite_space(this);
}

void BSharpParser::White_spaceContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitWhite_space(this);
}


antlrcpp::Any BSharpParser::White_spaceContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitWhite_space(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::White_spaceContext* BSharpParser::white_space() {
  White_spaceContext *_localctx = _tracker.createInstance<White_spaceContext>(_ctx, getState());
  enterRule(_localctx, 52, BSharpParser::RuleWhite_space);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(247);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 17, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(245);
        _errHandler->sync(this);
        switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 16, _ctx)) {
        case 1: {
          setState(242);
          comment();
          break;
        }

        case 2: {
          setState(243);
          match(BSharpParser::MULTILINE_COMMENT);
          break;
        }

        case 3: {
          setState(244);
          match(BSharpParser::WHITE_SPACE);
          break;
        }

        } 
      }
      setState(249);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 17, _ctx);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CommentContext ------------------------------------------------------------------

BSharpParser::CommentContext::CommentContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* BSharpParser::CommentContext::LINE_COMMENT() {
  return getToken(BSharpParser::LINE_COMMENT, 0);
}

tree::TerminalNode* BSharpParser::CommentContext::MULTILINE_COMMENT() {
  return getToken(BSharpParser::MULTILINE_COMMENT, 0);
}


size_t BSharpParser::CommentContext::getRuleIndex() const {
  return BSharpParser::RuleComment;
}

void BSharpParser::CommentContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterComment(this);
}

void BSharpParser::CommentContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<BSharpListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitComment(this);
}


antlrcpp::Any BSharpParser::CommentContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<BSharpVisitor*>(visitor))
    return parserVisitor->visitComment(this);
  else
    return visitor->visitChildren(this);
}

BSharpParser::CommentContext* BSharpParser::comment() {
  CommentContext *_localctx = _tracker.createInstance<CommentContext>(_ctx, getState());
  enterRule(_localctx, 54, BSharpParser::RuleComment);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(250);
    _la = _input->LA(1);
    if (!(_la == BSharpParser::LINE_COMMENT

    || _la == BSharpParser::MULTILINE_COMMENT)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

// Static vars and initialization.
std::vector<dfa::DFA> BSharpParser::_decisionToDFA;
atn::PredictionContextCache BSharpParser::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN BSharpParser::_atn;
std::vector<uint16_t> BSharpParser::_serializedATN;

std::vector<std::string> BSharpParser::_ruleNames = {
  "program", "declaration_expression", "import_statement", "function_declaration", 
  "variable_declaration", "structure_declaration", "class_declaration", 
  "class_header", "class_name", "class_body", "expression_class", "function_heading", 
  "function_parameters", "function_parameter", "function_body", "parameter_name", 
  "return_statement", "function_name", "function_expression", "function_call", 
  "valued_expression", "type_name", "assignment", "variable_name", "string", 
  "white_space1", "white_space", "comment"
};

std::vector<std::string> BSharpParser::_literalNames = {
  "", "", "", "", "'class'", "'return'", "'import'", "", "", "", "'('", 
  "')'", "'{'", "'}'", "';'", "','", "'='"
};

std::vector<std::string> BSharpParser::_symbolicNames = {
  "", "LINE_COMMENT", "MULTILINE_COMMENT", "STRING", "CLASS_TEXT", "RETURN_TEXT", 
  "IMPORT_TEXT", "NUMBER", "LABEL", "WHITE_SPACE", "OPEN_PAREN", "CLOSE_PAREN", 
  "OPEN_BRACKET", "CLOSE_BRACKET", "SEMICOLON", "COMMA", "EQUAL"
};

dfa::Vocabulary BSharpParser::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> BSharpParser::_tokenNames;

BSharpParser::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  _serializedATN = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964, 
    0x3, 0x12, 0xff, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x4, 0x4, 0x9, 
    0x4, 0x4, 0x5, 0x9, 0x5, 0x4, 0x6, 0x9, 0x6, 0x4, 0x7, 0x9, 0x7, 0x4, 
    0x8, 0x9, 0x8, 0x4, 0x9, 0x9, 0x9, 0x4, 0xa, 0x9, 0xa, 0x4, 0xb, 0x9, 
    0xb, 0x4, 0xc, 0x9, 0xc, 0x4, 0xd, 0x9, 0xd, 0x4, 0xe, 0x9, 0xe, 0x4, 
    0xf, 0x9, 0xf, 0x4, 0x10, 0x9, 0x10, 0x4, 0x11, 0x9, 0x11, 0x4, 0x12, 
    0x9, 0x12, 0x4, 0x13, 0x9, 0x13, 0x4, 0x14, 0x9, 0x14, 0x4, 0x15, 0x9, 
    0x15, 0x4, 0x16, 0x9, 0x16, 0x4, 0x17, 0x9, 0x17, 0x4, 0x18, 0x9, 0x18, 
    0x4, 0x19, 0x9, 0x19, 0x4, 0x1a, 0x9, 0x1a, 0x4, 0x1b, 0x9, 0x1b, 0x4, 
    0x1c, 0x9, 0x1c, 0x4, 0x1d, 0x9, 0x1d, 0x3, 0x2, 0x3, 0x2, 0x3, 0x2, 
    0x3, 0x2, 0x6, 0x2, 0x3f, 0xa, 0x2, 0xd, 0x2, 0xe, 0x2, 0x40, 0x3, 0x2, 
    0x3, 0x2, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x5, 0x3, 0x49, 0xa, 
    0x3, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 
    0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 
    0x6, 0x3, 0x6, 0x3, 0x6, 0x5, 0x6, 0x5b, 0xa, 0x6, 0x3, 0x6, 0x3, 0x6, 
    0x3, 0x6, 0x3, 0x7, 0x3, 0x7, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 
    0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x3, 0xa, 0x3, 0xa, 0x3, 0xb, 
    0x3, 0xb, 0x3, 0xb, 0x3, 0xb, 0x3, 0xb, 0x7, 0xb, 0x71, 0xa, 0xb, 0xc, 
    0xb, 0xe, 0xb, 0x74, 0xb, 0xb, 0x3, 0xb, 0x3, 0xb, 0x3, 0xc, 0x3, 0xc, 
    0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 
    0x3, 0xd, 0x3, 0xd, 0x5, 0xd, 0x83, 0xa, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 
    0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x7, 0xe, 0x8d, 
    0xa, 0xe, 0xc, 0xe, 0xe, 0xe, 0x90, 0xb, 0xe, 0x3, 0xf, 0x3, 0xf, 0x3, 
    0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x5, 0xf, 0x9a, 
    0xa, 0xf, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 
    0x10, 0x3, 0x10, 0x7, 0x10, 0xa3, 0xa, 0x10, 0xc, 0x10, 0xe, 0x10, 0xa6, 
    0xb, 0x10, 0x3, 0x10, 0x5, 0x10, 0xa9, 0xa, 0x10, 0x3, 0x10, 0x3, 0x10, 
    0x3, 0x10, 0x3, 0x11, 0x3, 0x11, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 
    0x12, 0x5, 0x12, 0xb4, 0xa, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 
    0x13, 0x3, 0x13, 0x3, 0x14, 0x3, 0x14, 0x3, 0x14, 0x5, 0x14, 0xbe, 0xa, 
    0x14, 0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 
    0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 0x7, 0x15, 0xca, 0xa, 0x15, 
    0xc, 0x15, 0xe, 0x15, 0xcd, 0xb, 0x15, 0x3, 0x15, 0x3, 0x15, 0x5, 0x15, 
    0xd1, 0xa, 0x15, 0x3, 0x15, 0x3, 0x15, 0x3, 0x16, 0x3, 0x16, 0x3, 0x17, 
    0x3, 0x17, 0x3, 0x18, 0x3, 0x18, 0x3, 0x18, 0x3, 0x18, 0x3, 0x18, 0x3, 
    0x18, 0x3, 0x18, 0x3, 0x19, 0x3, 0x19, 0x3, 0x1a, 0x3, 0x1a, 0x3, 0x1b, 
    0x7, 0x1b, 0xe5, 0xa, 0x1b, 0xc, 0x1b, 0xe, 0x1b, 0xe8, 0xb, 0x1b, 0x3, 
    0x1b, 0x3, 0x1b, 0x7, 0x1b, 0xec, 0xa, 0x1b, 0xc, 0x1b, 0xe, 0x1b, 0xef, 
    0xb, 0x1b, 0x6, 0x1b, 0xf1, 0xa, 0x1b, 0xd, 0x1b, 0xe, 0x1b, 0xf2, 0x3, 
    0x1c, 0x3, 0x1c, 0x3, 0x1c, 0x7, 0x1c, 0xf8, 0xa, 0x1c, 0xc, 0x1c, 0xe, 
    0x1c, 0xfb, 0xb, 0x1c, 0x3, 0x1d, 0x3, 0x1d, 0x3, 0x1d, 0x2, 0x2, 0x1e, 
    0x2, 0x4, 0x6, 0x8, 0xa, 0xc, 0xe, 0x10, 0x12, 0x14, 0x16, 0x18, 0x1a, 
    0x1c, 0x1e, 0x20, 0x22, 0x24, 0x26, 0x28, 0x2a, 0x2c, 0x2e, 0x30, 0x32, 
    0x34, 0x36, 0x38, 0x2, 0x3, 0x3, 0x2, 0x3, 0x4, 0x2, 0xf8, 0x2, 0x3e, 
    0x3, 0x2, 0x2, 0x2, 0x4, 0x48, 0x3, 0x2, 0x2, 0x2, 0x6, 0x4a, 0x3, 0x2, 
    0x2, 0x2, 0x8, 0x50, 0x3, 0x2, 0x2, 0x2, 0xa, 0x54, 0x3, 0x2, 0x2, 0x2, 
    0xc, 0x5f, 0x3, 0x2, 0x2, 0x2, 0xe, 0x61, 0x3, 0x2, 0x2, 0x2, 0x10, 
    0x65, 0x3, 0x2, 0x2, 0x2, 0x12, 0x69, 0x3, 0x2, 0x2, 0x2, 0x14, 0x6b, 
    0x3, 0x2, 0x2, 0x2, 0x16, 0x77, 0x3, 0x2, 0x2, 0x2, 0x18, 0x79, 0x3, 
    0x2, 0x2, 0x2, 0x1a, 0x86, 0x3, 0x2, 0x2, 0x2, 0x1c, 0x91, 0x3, 0x2, 
    0x2, 0x2, 0x1e, 0x9b, 0x3, 0x2, 0x2, 0x2, 0x20, 0xad, 0x3, 0x2, 0x2, 
    0x2, 0x22, 0xaf, 0x3, 0x2, 0x2, 0x2, 0x24, 0xb8, 0x3, 0x2, 0x2, 0x2, 
    0x26, 0xbd, 0x3, 0x2, 0x2, 0x2, 0x28, 0xbf, 0x3, 0x2, 0x2, 0x2, 0x2a, 
    0xd4, 0x3, 0x2, 0x2, 0x2, 0x2c, 0xd6, 0x3, 0x2, 0x2, 0x2, 0x2e, 0xd8, 
    0x3, 0x2, 0x2, 0x2, 0x30, 0xdf, 0x3, 0x2, 0x2, 0x2, 0x32, 0xe1, 0x3, 
    0x2, 0x2, 0x2, 0x34, 0xf0, 0x3, 0x2, 0x2, 0x2, 0x36, 0xf9, 0x3, 0x2, 
    0x2, 0x2, 0x38, 0xfc, 0x3, 0x2, 0x2, 0x2, 0x3a, 0x3b, 0x5, 0x36, 0x1c, 
    0x2, 0x3b, 0x3c, 0x5, 0x4, 0x3, 0x2, 0x3c, 0x3d, 0x5, 0x36, 0x1c, 0x2, 
    0x3d, 0x3f, 0x3, 0x2, 0x2, 0x2, 0x3e, 0x3a, 0x3, 0x2, 0x2, 0x2, 0x3f, 
    0x40, 0x3, 0x2, 0x2, 0x2, 0x40, 0x3e, 0x3, 0x2, 0x2, 0x2, 0x40, 0x41, 
    0x3, 0x2, 0x2, 0x2, 0x41, 0x42, 0x3, 0x2, 0x2, 0x2, 0x42, 0x43, 0x7, 
    0x2, 0x2, 0x3, 0x43, 0x3, 0x3, 0x2, 0x2, 0x2, 0x44, 0x49, 0x5, 0x6, 
    0x4, 0x2, 0x45, 0x49, 0x5, 0xa, 0x6, 0x2, 0x46, 0x49, 0x5, 0x8, 0x5, 
    0x2, 0x47, 0x49, 0x5, 0xc, 0x7, 0x2, 0x48, 0x44, 0x3, 0x2, 0x2, 0x2, 
    0x48, 0x45, 0x3, 0x2, 0x2, 0x2, 0x48, 0x46, 0x3, 0x2, 0x2, 0x2, 0x48, 
    0x47, 0x3, 0x2, 0x2, 0x2, 0x49, 0x5, 0x3, 0x2, 0x2, 0x2, 0x4a, 0x4b, 
    0x7, 0x8, 0x2, 0x2, 0x4b, 0x4c, 0x5, 0x34, 0x1b, 0x2, 0x4c, 0x4d, 0x5, 
    0x32, 0x1a, 0x2, 0x4d, 0x4e, 0x5, 0x36, 0x1c, 0x2, 0x4e, 0x4f, 0x7, 
    0x10, 0x2, 0x2, 0x4f, 0x7, 0x3, 0x2, 0x2, 0x2, 0x50, 0x51, 0x5, 0x18, 
    0xd, 0x2, 0x51, 0x52, 0x5, 0x36, 0x1c, 0x2, 0x52, 0x53, 0x5, 0x1e, 0x10, 
    0x2, 0x53, 0x9, 0x3, 0x2, 0x2, 0x2, 0x54, 0x5a, 0x5, 0x30, 0x19, 0x2, 
    0x55, 0x56, 0x5, 0x36, 0x1c, 0x2, 0x56, 0x57, 0x7, 0x12, 0x2, 0x2, 0x57, 
    0x58, 0x5, 0x36, 0x1c, 0x2, 0x58, 0x59, 0x5, 0x2a, 0x16, 0x2, 0x59, 
    0x5b, 0x3, 0x2, 0x2, 0x2, 0x5a, 0x55, 0x3, 0x2, 0x2, 0x2, 0x5a, 0x5b, 
    0x3, 0x2, 0x2, 0x2, 0x5b, 0x5c, 0x3, 0x2, 0x2, 0x2, 0x5c, 0x5d, 0x5, 
    0x36, 0x1c, 0x2, 0x5d, 0x5e, 0x7, 0x10, 0x2, 0x2, 0x5e, 0xb, 0x3, 0x2, 
    0x2, 0x2, 0x5f, 0x60, 0x5, 0xe, 0x8, 0x2, 0x60, 0xd, 0x3, 0x2, 0x2, 
    0x2, 0x61, 0x62, 0x5, 0x10, 0x9, 0x2, 0x62, 0x63, 0x5, 0x36, 0x1c, 0x2, 
    0x63, 0x64, 0x5, 0x14, 0xb, 0x2, 0x64, 0xf, 0x3, 0x2, 0x2, 0x2, 0x65, 
    0x66, 0x7, 0x6, 0x2, 0x2, 0x66, 0x67, 0x5, 0x36, 0x1c, 0x2, 0x67, 0x68, 
    0x5, 0x12, 0xa, 0x2, 0x68, 0x11, 0x3, 0x2, 0x2, 0x2, 0x69, 0x6a, 0x7, 
    0xa, 0x2, 0x2, 0x6a, 0x13, 0x3, 0x2, 0x2, 0x2, 0x6b, 0x6c, 0x7, 0xe, 
    0x2, 0x2, 0x6c, 0x72, 0x5, 0x36, 0x1c, 0x2, 0x6d, 0x6e, 0x5, 0x16, 0xc, 
    0x2, 0x6e, 0x6f, 0x5, 0x36, 0x1c, 0x2, 0x6f, 0x71, 0x3, 0x2, 0x2, 0x2, 
    0x70, 0x6d, 0x3, 0x2, 0x2, 0x2, 0x71, 0x74, 0x3, 0x2, 0x2, 0x2, 0x72, 
    0x70, 0x3, 0x2, 0x2, 0x2, 0x72, 0x73, 0x3, 0x2, 0x2, 0x2, 0x73, 0x75, 
    0x3, 0x2, 0x2, 0x2, 0x74, 0x72, 0x3, 0x2, 0x2, 0x2, 0x75, 0x76, 0x7, 
    0xf, 0x2, 0x2, 0x76, 0x15, 0x3, 0x2, 0x2, 0x2, 0x77, 0x78, 0x5, 0x4, 
    0x3, 0x2, 0x78, 0x17, 0x3, 0x2, 0x2, 0x2, 0x79, 0x7a, 0x5, 0x2c, 0x17, 
    0x2, 0x7a, 0x7b, 0x5, 0x34, 0x1b, 0x2, 0x7b, 0x7c, 0x5, 0x24, 0x13, 
    0x2, 0x7c, 0x7d, 0x5, 0x36, 0x1c, 0x2, 0x7d, 0x7e, 0x7, 0xc, 0x2, 0x2, 
    0x7e, 0x82, 0x5, 0x36, 0x1c, 0x2, 0x7f, 0x80, 0x5, 0x1a, 0xe, 0x2, 0x80, 
    0x81, 0x5, 0x36, 0x1c, 0x2, 0x81, 0x83, 0x3, 0x2, 0x2, 0x2, 0x82, 0x7f, 
    0x3, 0x2, 0x2, 0x2, 0x82, 0x83, 0x3, 0x2, 0x2, 0x2, 0x83, 0x84, 0x3, 
    0x2, 0x2, 0x2, 0x84, 0x85, 0x7, 0xd, 0x2, 0x2, 0x85, 0x19, 0x3, 0x2, 
    0x2, 0x2, 0x86, 0x8e, 0x5, 0x1c, 0xf, 0x2, 0x87, 0x88, 0x5, 0x36, 0x1c, 
    0x2, 0x88, 0x89, 0x7, 0x11, 0x2, 0x2, 0x89, 0x8a, 0x5, 0x36, 0x1c, 0x2, 
    0x8a, 0x8b, 0x5, 0x1c, 0xf, 0x2, 0x8b, 0x8d, 0x3, 0x2, 0x2, 0x2, 0x8c, 
    0x87, 0x3, 0x2, 0x2, 0x2, 0x8d, 0x90, 0x3, 0x2, 0x2, 0x2, 0x8e, 0x8c, 
    0x3, 0x2, 0x2, 0x2, 0x8e, 0x8f, 0x3, 0x2, 0x2, 0x2, 0x8f, 0x1b, 0x3, 
    0x2, 0x2, 0x2, 0x90, 0x8e, 0x3, 0x2, 0x2, 0x2, 0x91, 0x92, 0x5, 0x2c, 
    0x17, 0x2, 0x92, 0x93, 0x5, 0x34, 0x1b, 0x2, 0x93, 0x99, 0x5, 0x20, 
    0x11, 0x2, 0x94, 0x95, 0x5, 0x36, 0x1c, 0x2, 0x95, 0x96, 0x7, 0x12, 
    0x2, 0x2, 0x96, 0x97, 0x5, 0x36, 0x1c, 0x2, 0x97, 0x98, 0x5, 0x2a, 0x16, 
    0x2, 0x98, 0x9a, 0x3, 0x2, 0x2, 0x2, 0x99, 0x94, 0x3, 0x2, 0x2, 0x2, 
    0x99, 0x9a, 0x3, 0x2, 0x2, 0x2, 0x9a, 0x1d, 0x3, 0x2, 0x2, 0x2, 0x9b, 
    0x9c, 0x7, 0xe, 0x2, 0x2, 0x9c, 0xa4, 0x5, 0x36, 0x1c, 0x2, 0x9d, 0x9e, 
    0x5, 0x26, 0x14, 0x2, 0x9e, 0x9f, 0x5, 0x36, 0x1c, 0x2, 0x9f, 0xa0, 
    0x7, 0x10, 0x2, 0x2, 0xa0, 0xa1, 0x5, 0x36, 0x1c, 0x2, 0xa1, 0xa3, 0x3, 
    0x2, 0x2, 0x2, 0xa2, 0x9d, 0x3, 0x2, 0x2, 0x2, 0xa3, 0xa6, 0x3, 0x2, 
    0x2, 0x2, 0xa4, 0xa2, 0x3, 0x2, 0x2, 0x2, 0xa4, 0xa5, 0x3, 0x2, 0x2, 
    0x2, 0xa5, 0xa8, 0x3, 0x2, 0x2, 0x2, 0xa6, 0xa4, 0x3, 0x2, 0x2, 0x2, 
    0xa7, 0xa9, 0x5, 0x22, 0x12, 0x2, 0xa8, 0xa7, 0x3, 0x2, 0x2, 0x2, 0xa8, 
    0xa9, 0x3, 0x2, 0x2, 0x2, 0xa9, 0xaa, 0x3, 0x2, 0x2, 0x2, 0xaa, 0xab, 
    0x5, 0x36, 0x1c, 0x2, 0xab, 0xac, 0x7, 0xf, 0x2, 0x2, 0xac, 0x1f, 0x3, 
    0x2, 0x2, 0x2, 0xad, 0xae, 0x7, 0xa, 0x2, 0x2, 0xae, 0x21, 0x3, 0x2, 
    0x2, 0x2, 0xaf, 0xb3, 0x7, 0x7, 0x2, 0x2, 0xb0, 0xb1, 0x5, 0x34, 0x1b, 
    0x2, 0xb1, 0xb2, 0x5, 0x2a, 0x16, 0x2, 0xb2, 0xb4, 0x3, 0x2, 0x2, 0x2, 
    0xb3, 0xb0, 0x3, 0x2, 0x2, 0x2, 0xb3, 0xb4, 0x3, 0x2, 0x2, 0x2, 0xb4, 
    0xb5, 0x3, 0x2, 0x2, 0x2, 0xb5, 0xb6, 0x5, 0x36, 0x1c, 0x2, 0xb6, 0xb7, 
    0x7, 0x10, 0x2, 0x2, 0xb7, 0x23, 0x3, 0x2, 0x2, 0x2, 0xb8, 0xb9, 0x7, 
    0xa, 0x2, 0x2, 0xb9, 0x25, 0x3, 0x2, 0x2, 0x2, 0xba, 0xbe, 0x5, 0x4, 
    0x3, 0x2, 0xbb, 0xbe, 0x5, 0x2a, 0x16, 0x2, 0xbc, 0xbe, 0x5, 0x2e, 0x18, 
    0x2, 0xbd, 0xba, 0x3, 0x2, 0x2, 0x2, 0xbd, 0xbb, 0x3, 0x2, 0x2, 0x2, 
    0xbd, 0xbc, 0x3, 0x2, 0x2, 0x2, 0xbe, 0x27, 0x3, 0x2, 0x2, 0x2, 0xbf, 
    0xc0, 0x5, 0x24, 0x13, 0x2, 0xc0, 0xc1, 0x5, 0x36, 0x1c, 0x2, 0xc1, 
    0xd0, 0x7, 0xc, 0x2, 0x2, 0xc2, 0xc3, 0x5, 0x36, 0x1c, 0x2, 0xc3, 0xcb, 
    0x5, 0x2a, 0x16, 0x2, 0xc4, 0xc5, 0x5, 0x36, 0x1c, 0x2, 0xc5, 0xc6, 
    0x7, 0x11, 0x2, 0x2, 0xc6, 0xc7, 0x5, 0x36, 0x1c, 0x2, 0xc7, 0xc8, 0x5, 
    0x2a, 0x16, 0x2, 0xc8, 0xca, 0x3, 0x2, 0x2, 0x2, 0xc9, 0xc4, 0x3, 0x2, 
    0x2, 0x2, 0xca, 0xcd, 0x3, 0x2, 0x2, 0x2, 0xcb, 0xc9, 0x3, 0x2, 0x2, 
    0x2, 0xcb, 0xcc, 0x3, 0x2, 0x2, 0x2, 0xcc, 0xce, 0x3, 0x2, 0x2, 0x2, 
    0xcd, 0xcb, 0x3, 0x2, 0x2, 0x2, 0xce, 0xcf, 0x5, 0x36, 0x1c, 0x2, 0xcf, 
    0xd1, 0x3, 0x2, 0x2, 0x2, 0xd0, 0xc2, 0x3, 0x2, 0x2, 0x2, 0xd0, 0xd1, 
    0x3, 0x2, 0x2, 0x2, 0xd1, 0xd2, 0x3, 0x2, 0x2, 0x2, 0xd2, 0xd3, 0x7, 
    0xd, 0x2, 0x2, 0xd3, 0x29, 0x3, 0x2, 0x2, 0x2, 0xd4, 0xd5, 0x5, 0x28, 
    0x15, 0x2, 0xd5, 0x2b, 0x3, 0x2, 0x2, 0x2, 0xd6, 0xd7, 0x7, 0xa, 0x2, 
    0x2, 0xd7, 0x2d, 0x3, 0x2, 0x2, 0x2, 0xd8, 0xd9, 0x5, 0x30, 0x19, 0x2, 
    0xd9, 0xda, 0x5, 0x34, 0x1b, 0x2, 0xda, 0xdb, 0x7, 0x12, 0x2, 0x2, 0xdb, 
    0xdc, 0x5, 0x2a, 0x16, 0x2, 0xdc, 0xdd, 0x5, 0x36, 0x1c, 0x2, 0xdd, 
    0xde, 0x7, 0x10, 0x2, 0x2, 0xde, 0x2f, 0x3, 0x2, 0x2, 0x2, 0xdf, 0xe0, 
    0x7, 0xa, 0x2, 0x2, 0xe0, 0x31, 0x3, 0x2, 0x2, 0x2, 0xe1, 0xe2, 0x7, 
    0x5, 0x2, 0x2, 0xe2, 0x33, 0x3, 0x2, 0x2, 0x2, 0xe3, 0xe5, 0x5, 0x38, 
    0x1d, 0x2, 0xe4, 0xe3, 0x3, 0x2, 0x2, 0x2, 0xe5, 0xe8, 0x3, 0x2, 0x2, 
    0x2, 0xe6, 0xe4, 0x3, 0x2, 0x2, 0x2, 0xe6, 0xe7, 0x3, 0x2, 0x2, 0x2, 
    0xe7, 0xe9, 0x3, 0x2, 0x2, 0x2, 0xe8, 0xe6, 0x3, 0x2, 0x2, 0x2, 0xe9, 
    0xed, 0x7, 0xb, 0x2, 0x2, 0xea, 0xec, 0x5, 0x38, 0x1d, 0x2, 0xeb, 0xea, 
    0x3, 0x2, 0x2, 0x2, 0xec, 0xef, 0x3, 0x2, 0x2, 0x2, 0xed, 0xeb, 0x3, 
    0x2, 0x2, 0x2, 0xed, 0xee, 0x3, 0x2, 0x2, 0x2, 0xee, 0xf1, 0x3, 0x2, 
    0x2, 0x2, 0xef, 0xed, 0x3, 0x2, 0x2, 0x2, 0xf0, 0xe6, 0x3, 0x2, 0x2, 
    0x2, 0xf1, 0xf2, 0x3, 0x2, 0x2, 0x2, 0xf2, 0xf0, 0x3, 0x2, 0x2, 0x2, 
    0xf2, 0xf3, 0x3, 0x2, 0x2, 0x2, 0xf3, 0x35, 0x3, 0x2, 0x2, 0x2, 0xf4, 
    0xf8, 0x5, 0x38, 0x1d, 0x2, 0xf5, 0xf8, 0x7, 0x4, 0x2, 0x2, 0xf6, 0xf8, 
    0x7, 0xb, 0x2, 0x2, 0xf7, 0xf4, 0x3, 0x2, 0x2, 0x2, 0xf7, 0xf5, 0x3, 
    0x2, 0x2, 0x2, 0xf7, 0xf6, 0x3, 0x2, 0x2, 0x2, 0xf8, 0xfb, 0x3, 0x2, 
    0x2, 0x2, 0xf9, 0xf7, 0x3, 0x2, 0x2, 0x2, 0xf9, 0xfa, 0x3, 0x2, 0x2, 
    0x2, 0xfa, 0x37, 0x3, 0x2, 0x2, 0x2, 0xfb, 0xf9, 0x3, 0x2, 0x2, 0x2, 
    0xfc, 0xfd, 0x9, 0x2, 0x2, 0x2, 0xfd, 0x39, 0x3, 0x2, 0x2, 0x2, 0x14, 
    0x40, 0x48, 0x5a, 0x72, 0x82, 0x8e, 0x99, 0xa4, 0xa8, 0xb3, 0xbd, 0xcb, 
    0xd0, 0xe6, 0xed, 0xf2, 0xf7, 0xf9, 
  };

  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

BSharpParser::Initializer BSharpParser::_init;
