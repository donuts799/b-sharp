
// Generated from ..\\BSharp.g4 by ANTLR 4.8

#pragma once


#include "antlr4-runtime.h"
#include "BSharpParser.h"


/**
 * This interface defines an abstract listener for a parse tree produced by BSharpParser.
 */
class  BSharpListener : public antlr4::tree::ParseTreeListener {
public:

  virtual void enterProgram(BSharpParser::ProgramContext *ctx) = 0;
  virtual void exitProgram(BSharpParser::ProgramContext *ctx) = 0;

  virtual void enterDeclaration_expression(BSharpParser::Declaration_expressionContext *ctx) = 0;
  virtual void exitDeclaration_expression(BSharpParser::Declaration_expressionContext *ctx) = 0;

  virtual void enterImport_statement(BSharpParser::Import_statementContext *ctx) = 0;
  virtual void exitImport_statement(BSharpParser::Import_statementContext *ctx) = 0;

  virtual void enterFunction_declaration(BSharpParser::Function_declarationContext *ctx) = 0;
  virtual void exitFunction_declaration(BSharpParser::Function_declarationContext *ctx) = 0;

  virtual void enterVariable_declaration(BSharpParser::Variable_declarationContext *ctx) = 0;
  virtual void exitVariable_declaration(BSharpParser::Variable_declarationContext *ctx) = 0;

  virtual void enterStructure_declaration(BSharpParser::Structure_declarationContext *ctx) = 0;
  virtual void exitStructure_declaration(BSharpParser::Structure_declarationContext *ctx) = 0;

  virtual void enterClass_declaration(BSharpParser::Class_declarationContext *ctx) = 0;
  virtual void exitClass_declaration(BSharpParser::Class_declarationContext *ctx) = 0;

  virtual void enterClass_header(BSharpParser::Class_headerContext *ctx) = 0;
  virtual void exitClass_header(BSharpParser::Class_headerContext *ctx) = 0;

  virtual void enterClass_name(BSharpParser::Class_nameContext *ctx) = 0;
  virtual void exitClass_name(BSharpParser::Class_nameContext *ctx) = 0;

  virtual void enterClass_body(BSharpParser::Class_bodyContext *ctx) = 0;
  virtual void exitClass_body(BSharpParser::Class_bodyContext *ctx) = 0;

  virtual void enterExpression_class(BSharpParser::Expression_classContext *ctx) = 0;
  virtual void exitExpression_class(BSharpParser::Expression_classContext *ctx) = 0;

  virtual void enterFunction_heading(BSharpParser::Function_headingContext *ctx) = 0;
  virtual void exitFunction_heading(BSharpParser::Function_headingContext *ctx) = 0;

  virtual void enterFunction_parameters(BSharpParser::Function_parametersContext *ctx) = 0;
  virtual void exitFunction_parameters(BSharpParser::Function_parametersContext *ctx) = 0;

  virtual void enterFunction_parameter(BSharpParser::Function_parameterContext *ctx) = 0;
  virtual void exitFunction_parameter(BSharpParser::Function_parameterContext *ctx) = 0;

  virtual void enterFunction_body(BSharpParser::Function_bodyContext *ctx) = 0;
  virtual void exitFunction_body(BSharpParser::Function_bodyContext *ctx) = 0;

  virtual void enterParameter_name(BSharpParser::Parameter_nameContext *ctx) = 0;
  virtual void exitParameter_name(BSharpParser::Parameter_nameContext *ctx) = 0;

  virtual void enterReturn_statement(BSharpParser::Return_statementContext *ctx) = 0;
  virtual void exitReturn_statement(BSharpParser::Return_statementContext *ctx) = 0;

  virtual void enterFunction_name(BSharpParser::Function_nameContext *ctx) = 0;
  virtual void exitFunction_name(BSharpParser::Function_nameContext *ctx) = 0;

  virtual void enterFunction_expression(BSharpParser::Function_expressionContext *ctx) = 0;
  virtual void exitFunction_expression(BSharpParser::Function_expressionContext *ctx) = 0;

  virtual void enterFunction_call(BSharpParser::Function_callContext *ctx) = 0;
  virtual void exitFunction_call(BSharpParser::Function_callContext *ctx) = 0;

  virtual void enterValued_expression(BSharpParser::Valued_expressionContext *ctx) = 0;
  virtual void exitValued_expression(BSharpParser::Valued_expressionContext *ctx) = 0;

  virtual void enterType_name(BSharpParser::Type_nameContext *ctx) = 0;
  virtual void exitType_name(BSharpParser::Type_nameContext *ctx) = 0;

  virtual void enterAssignment(BSharpParser::AssignmentContext *ctx) = 0;
  virtual void exitAssignment(BSharpParser::AssignmentContext *ctx) = 0;

  virtual void enterVariable_name(BSharpParser::Variable_nameContext *ctx) = 0;
  virtual void exitVariable_name(BSharpParser::Variable_nameContext *ctx) = 0;

  virtual void enterString(BSharpParser::StringContext *ctx) = 0;
  virtual void exitString(BSharpParser::StringContext *ctx) = 0;

  virtual void enterWhite_space1(BSharpParser::White_space1Context *ctx) = 0;
  virtual void exitWhite_space1(BSharpParser::White_space1Context *ctx) = 0;

  virtual void enterWhite_space(BSharpParser::White_spaceContext *ctx) = 0;
  virtual void exitWhite_space(BSharpParser::White_spaceContext *ctx) = 0;

  virtual void enterComment(BSharpParser::CommentContext *ctx) = 0;
  virtual void exitComment(BSharpParser::CommentContext *ctx) = 0;


};

