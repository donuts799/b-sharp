
// Generated from ..\\BSharp.g4 by ANTLR 4.8

#pragma once


#include "antlr4-runtime.h"
#include "BSharpParser.h"



/**
 * This class defines an abstract visitor for a parse tree
 * produced by BSharpParser.
 */
class  BSharpVisitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by BSharpParser.
   */
    virtual antlrcpp::Any visitProgram(BSharpParser::ProgramContext *context) = 0;

    virtual antlrcpp::Any visitDeclaration_expression(BSharpParser::Declaration_expressionContext *context) = 0;

    virtual antlrcpp::Any visitImport_statement(BSharpParser::Import_statementContext *context) = 0;

    virtual antlrcpp::Any visitFunction_declaration(BSharpParser::Function_declarationContext *context) = 0;

    virtual antlrcpp::Any visitVariable_declaration(BSharpParser::Variable_declarationContext *context) = 0;

    virtual antlrcpp::Any visitStructure_declaration(BSharpParser::Structure_declarationContext *context) = 0;

    virtual antlrcpp::Any visitClass_declaration(BSharpParser::Class_declarationContext *context) = 0;

    virtual antlrcpp::Any visitClass_header(BSharpParser::Class_headerContext *context) = 0;

    virtual antlrcpp::Any visitClass_name(BSharpParser::Class_nameContext *context) = 0;

    virtual antlrcpp::Any visitClass_body(BSharpParser::Class_bodyContext *context) = 0;

    virtual antlrcpp::Any visitExpression_class(BSharpParser::Expression_classContext *context) = 0;

    virtual antlrcpp::Any visitFunction_heading(BSharpParser::Function_headingContext *context) = 0;

    virtual antlrcpp::Any visitFunction_parameters(BSharpParser::Function_parametersContext *context) = 0;

    virtual antlrcpp::Any visitFunction_parameter(BSharpParser::Function_parameterContext *context) = 0;

    virtual antlrcpp::Any visitFunction_body(BSharpParser::Function_bodyContext *context) = 0;

    virtual antlrcpp::Any visitParameter_name(BSharpParser::Parameter_nameContext *context) = 0;

    virtual antlrcpp::Any visitReturn_statement(BSharpParser::Return_statementContext *context) = 0;

    virtual antlrcpp::Any visitFunction_name(BSharpParser::Function_nameContext *context) = 0;

    virtual antlrcpp::Any visitFunction_expression(BSharpParser::Function_expressionContext *context) = 0;

    virtual antlrcpp::Any visitFunction_call(BSharpParser::Function_callContext *context) = 0;

    virtual antlrcpp::Any visitValued_expression(BSharpParser::Valued_expressionContext *context) = 0;

    virtual antlrcpp::Any visitType_name(BSharpParser::Type_nameContext *context) = 0;

    virtual antlrcpp::Any visitAssignment(BSharpParser::AssignmentContext *context) = 0;

    virtual antlrcpp::Any visitVariable_name(BSharpParser::Variable_nameContext *context) = 0;

    virtual antlrcpp::Any visitString(BSharpParser::StringContext *context) = 0;

    virtual antlrcpp::Any visitWhite_space1(BSharpParser::White_space1Context *context) = 0;

    virtual antlrcpp::Any visitWhite_space(BSharpParser::White_spaceContext *context) = 0;

    virtual antlrcpp::Any visitComment(BSharpParser::CommentContext *context) = 0;


};

