
// Generated from ..\\BSharp.g4 by ANTLR 4.8

#pragma once


#include "antlr4-runtime.h"
#include "BSharpVisitor.h"


/**
 * This class provides an empty implementation of BSharpVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class  BSharpBaseVisitor : public BSharpVisitor {
public:

  virtual antlrcpp::Any visitProgram(BSharpParser::ProgramContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDeclaration_expression(BSharpParser::Declaration_expressionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitImport_statement(BSharpParser::Import_statementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFunction_declaration(BSharpParser::Function_declarationContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitVariable_declaration(BSharpParser::Variable_declarationContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitStructure_declaration(BSharpParser::Structure_declarationContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitClass_declaration(BSharpParser::Class_declarationContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitClass_header(BSharpParser::Class_headerContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitClass_name(BSharpParser::Class_nameContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitClass_body(BSharpParser::Class_bodyContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExpression_class(BSharpParser::Expression_classContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFunction_heading(BSharpParser::Function_headingContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFunction_parameters(BSharpParser::Function_parametersContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFunction_parameter(BSharpParser::Function_parameterContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFunction_body(BSharpParser::Function_bodyContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitParameter_name(BSharpParser::Parameter_nameContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitReturn_statement(BSharpParser::Return_statementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFunction_name(BSharpParser::Function_nameContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFunction_expression(BSharpParser::Function_expressionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFunction_call(BSharpParser::Function_callContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitValued_expression(BSharpParser::Valued_expressionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitType_name(BSharpParser::Type_nameContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitAssignment(BSharpParser::AssignmentContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitVariable_name(BSharpParser::Variable_nameContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitString(BSharpParser::StringContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitWhite_space1(BSharpParser::White_space1Context *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitWhite_space(BSharpParser::White_spaceContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitComment(BSharpParser::CommentContext *ctx) override {
    return visitChildren(ctx);
  }


};

