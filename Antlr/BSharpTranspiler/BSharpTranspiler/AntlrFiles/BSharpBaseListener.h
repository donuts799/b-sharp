
// Generated from ..\\BSharp.g4 by ANTLR 4.8

#pragma once


#include "antlr4-runtime.h"
#include "BSharpListener.h"


/**
 * This class provides an empty implementation of BSharpListener,
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
class  BSharpBaseListener : public BSharpListener {
public:

  virtual void enterProgram(BSharpParser::ProgramContext * /*ctx*/) override { }
  virtual void exitProgram(BSharpParser::ProgramContext * /*ctx*/) override { }

  virtual void enterDeclaration_expression(BSharpParser::Declaration_expressionContext * /*ctx*/) override { }
  virtual void exitDeclaration_expression(BSharpParser::Declaration_expressionContext * /*ctx*/) override { }

  virtual void enterImport_statement(BSharpParser::Import_statementContext * /*ctx*/) override { }
  virtual void exitImport_statement(BSharpParser::Import_statementContext * /*ctx*/) override { }

  virtual void enterFunction_declaration(BSharpParser::Function_declarationContext * /*ctx*/) override { }
  virtual void exitFunction_declaration(BSharpParser::Function_declarationContext * /*ctx*/) override { }

  virtual void enterVariable_declaration(BSharpParser::Variable_declarationContext * /*ctx*/) override { }
  virtual void exitVariable_declaration(BSharpParser::Variable_declarationContext * /*ctx*/) override { }

  virtual void enterStructure_declaration(BSharpParser::Structure_declarationContext * /*ctx*/) override { }
  virtual void exitStructure_declaration(BSharpParser::Structure_declarationContext * /*ctx*/) override { }

  virtual void enterClass_declaration(BSharpParser::Class_declarationContext * /*ctx*/) override { }
  virtual void exitClass_declaration(BSharpParser::Class_declarationContext * /*ctx*/) override { }

  virtual void enterClass_header(BSharpParser::Class_headerContext * /*ctx*/) override { }
  virtual void exitClass_header(BSharpParser::Class_headerContext * /*ctx*/) override { }

  virtual void enterClass_name(BSharpParser::Class_nameContext * /*ctx*/) override { }
  virtual void exitClass_name(BSharpParser::Class_nameContext * /*ctx*/) override { }

  virtual void enterClass_body(BSharpParser::Class_bodyContext * /*ctx*/) override { }
  virtual void exitClass_body(BSharpParser::Class_bodyContext * /*ctx*/) override { }

  virtual void enterExpression_class(BSharpParser::Expression_classContext * /*ctx*/) override { }
  virtual void exitExpression_class(BSharpParser::Expression_classContext * /*ctx*/) override { }

  virtual void enterFunction_heading(BSharpParser::Function_headingContext * /*ctx*/) override { }
  virtual void exitFunction_heading(BSharpParser::Function_headingContext * /*ctx*/) override { }

  virtual void enterFunction_parameters(BSharpParser::Function_parametersContext * /*ctx*/) override { }
  virtual void exitFunction_parameters(BSharpParser::Function_parametersContext * /*ctx*/) override { }

  virtual void enterFunction_parameter(BSharpParser::Function_parameterContext * /*ctx*/) override { }
  virtual void exitFunction_parameter(BSharpParser::Function_parameterContext * /*ctx*/) override { }

  virtual void enterFunction_body(BSharpParser::Function_bodyContext * /*ctx*/) override { }
  virtual void exitFunction_body(BSharpParser::Function_bodyContext * /*ctx*/) override { }

  virtual void enterParameter_name(BSharpParser::Parameter_nameContext * /*ctx*/) override { }
  virtual void exitParameter_name(BSharpParser::Parameter_nameContext * /*ctx*/) override { }

  virtual void enterReturn_statement(BSharpParser::Return_statementContext * /*ctx*/) override { }
  virtual void exitReturn_statement(BSharpParser::Return_statementContext * /*ctx*/) override { }

  virtual void enterFunction_name(BSharpParser::Function_nameContext * /*ctx*/) override { }
  virtual void exitFunction_name(BSharpParser::Function_nameContext * /*ctx*/) override { }

  virtual void enterFunction_expression(BSharpParser::Function_expressionContext * /*ctx*/) override { }
  virtual void exitFunction_expression(BSharpParser::Function_expressionContext * /*ctx*/) override { }

  virtual void enterFunction_call(BSharpParser::Function_callContext * /*ctx*/) override { }
  virtual void exitFunction_call(BSharpParser::Function_callContext * /*ctx*/) override { }

  virtual void enterValued_expression(BSharpParser::Valued_expressionContext * /*ctx*/) override { }
  virtual void exitValued_expression(BSharpParser::Valued_expressionContext * /*ctx*/) override { }

  virtual void enterType_name(BSharpParser::Type_nameContext * /*ctx*/) override { }
  virtual void exitType_name(BSharpParser::Type_nameContext * /*ctx*/) override { }

  virtual void enterAssignment(BSharpParser::AssignmentContext * /*ctx*/) override { }
  virtual void exitAssignment(BSharpParser::AssignmentContext * /*ctx*/) override { }

  virtual void enterVariable_name(BSharpParser::Variable_nameContext * /*ctx*/) override { }
  virtual void exitVariable_name(BSharpParser::Variable_nameContext * /*ctx*/) override { }

  virtual void enterString(BSharpParser::StringContext * /*ctx*/) override { }
  virtual void exitString(BSharpParser::StringContext * /*ctx*/) override { }

  virtual void enterWhite_space1(BSharpParser::White_space1Context * /*ctx*/) override { }
  virtual void exitWhite_space1(BSharpParser::White_space1Context * /*ctx*/) override { }

  virtual void enterWhite_space(BSharpParser::White_spaceContext * /*ctx*/) override { }
  virtual void exitWhite_space(BSharpParser::White_spaceContext * /*ctx*/) override { }

  virtual void enterComment(BSharpParser::CommentContext * /*ctx*/) override { }
  virtual void exitComment(BSharpParser::CommentContext * /*ctx*/) override { }


  virtual void enterEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void exitEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void visitTerminal(antlr4::tree::TerminalNode * /*node*/) override { }
  virtual void visitErrorNode(antlr4::tree::ErrorNode * /*node*/) override { }

};

