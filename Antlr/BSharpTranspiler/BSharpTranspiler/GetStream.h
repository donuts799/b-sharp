#pragma once
#include <iostream>
#include "antlr4-runtime.h"

enum class StreamType
{
	String,
	File
};

std::shared_ptr<std::istream> getStream(StreamType streamType, std::string data);

