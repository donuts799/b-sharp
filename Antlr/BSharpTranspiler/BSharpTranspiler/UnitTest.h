#pragma once
#include "TestFramework.h"
class UnitTest
{
public:
	UnitTest();
	~UnitTest();
	void Test_CreateFileStream_Program();
	void Test_CreateStringStream_Program();
	void Test_CreateLexer_Program();
	void Test_CreateParser_Program();

	void Test_VisitProgram_Visitor();
private:
	TestFramework::Test testFramework;

};

extern UnitTest unitTest;