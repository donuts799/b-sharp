#include "UnitTest.h"
#include "GetStream.h"
#include "antlr4-runtime.h"
#include "BSharpLexer.h"
#include "BSharpParser.h"
#include "Visitor.h"

using namespace TestFramework;

UnitTest unitTest = UnitTest();

void UnitTest::Test_CreateFileStream_Program()
{
	try
	{
		getStream(StreamType::File, "TestInput.bs");


		Succeeded expression = Succeeded("Successfully created stream");
		testFramework.test(expression, "Test_CreateFileStream_Program", "Program");
	}
	catch (...)
	{
		Failed expression = Failed("Could not create stream for unknown reasons");
		testFramework.test(expression, "Test_CreateFileStream_Program", "Program");
	}
}

void UnitTest::Test_CreateStringStream_Program()
{
	try
	{
		getStream(StreamType::String, R"(import "thing";
// this is a comment
void foo()
{
})");


		Succeeded expression = Succeeded("Successfully created stream");
		testFramework.test(expression, "Test_CreateFileStream_Program", "Program");
	}
	catch (...)
	{
		Failed expression = Failed("Could not create stream for unknown reasons");
		testFramework.test(expression, "Test_CreateFileStream_Program", "Program");
	}
}

void UnitTest::Test_CreateLexer_Program()
{
	try
	{
		auto stream = getStream(StreamType::File, "TestInput.bs");

		antlr4::ANTLRInputStream input = antlr4::ANTLRInputStream(*stream);
		BSharpLexer lexer(&input);


		Succeeded expression = Succeeded("Successfully created lexer");
		testFramework.test(expression, __FUNCTION__, "Program");
	}
	catch (...)
	{
		Failed expression = Failed("Could not create lexer for unknown reasons");
		testFramework.test(expression, __FUNCTION__, "Program");
	}
}

void UnitTest::Test_CreateParser_Program()
{
	try
	{
		auto stream = getStream(StreamType::File, "TestInput.bs");

		antlr4::ANTLRInputStream input = antlr4::ANTLRInputStream(*stream);
		BSharpLexer lexer(&input);
		antlr4::CommonTokenStream tokens(&lexer);
		BSharpParser parser(&tokens);


		Succeeded expression = Succeeded("Successfully created Parser");
		testFramework.test(expression, __FUNCTION__, "Program");
	}
	catch (...)
	{
		Failed expression = Failed("Could not create Parser for unknown reasons");
		testFramework.test(expression, __FUNCTION__, "Program");
	}
}



void UnitTest::Test_VisitProgram_Visitor()
{
	try
	{
		auto stream = getStream(StreamType::String, R"(import "thing";
// this is a comment
void foo()
{
})");

		antlr4::ANTLRInputStream input = antlr4::ANTLRInputStream(*stream);
		BSharpLexer lexer(&input);
		antlr4::CommonTokenStream tokens(&lexer);
		BSharpParser parser(&tokens);
		BSharpParser::ProgramContext* tree = parser.program();
		Visitor visitor;
		antlrcpp::Any result = visitor.visitProgram(tree);
		
		
		IsNotNull expression = IsNotNull(result.isNull() ? NULL : (void*) 1);
		testFramework.test(expression, __FUNCTION__, "Visitor");

		

	}
	catch (...)
	{
		Failed expression = Failed("Could not visit BSharpProgram for unknown reasons");
		testFramework.test(expression, __FUNCTION__, "Visitor");
	}
}

UnitTest::UnitTest()
{
	Test_CreateFileStream_Program();
	Test_CreateStringStream_Program();
	Test_CreateLexer_Program();
	Test_CreateParser_Program();

	Test_VisitProgram_Visitor();
}

UnitTest::~UnitTest()
{

}