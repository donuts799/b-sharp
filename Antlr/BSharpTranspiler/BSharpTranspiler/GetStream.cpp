#include "GetStream.h"
#include <fstream>
#include <sstream>


std::shared_ptr<std::istream> getStream(StreamType streamType, std::string data)
{
	std::shared_ptr<std::istream> returnValue;
	switch (streamType)
	{
	case StreamType::File:
		returnValue = std::shared_ptr<std::istream>(new std::ifstream(data));
		break;
	case StreamType::String:
		returnValue = std::shared_ptr<std::istream>(new std::stringstream(data));
		break;
	default:
		throw std::exception("Invalid stream type");
		break;
	}

	if (!(*returnValue)) // stream is not valid/working
	{
		throw std::exception("Stream could not be validated");
	}

	return returnValue;
}